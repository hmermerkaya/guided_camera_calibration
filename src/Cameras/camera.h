////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///     Abstract camera class defining the interface for all devices         ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/data_types.h"

#include <QImage>
#include <QObject>

///
/// \brief The Camera class
///
class Camera: public QObject{
    Q_OBJECT

public slots:

    ///
    /// \brief getDeviceList is used to collect the device lists for different
    ///                      camera class implementations and give the user the
    ///                      opportunity to select among all available devices
    /// \return
    ///
    virtual QStringList getDeviceList() = 0;

    ///
    /// \brief init checks if the selected device is available and initializes
    ///             it
    /// \param idx  is the index of the device in the list of devices
    /// \return     true, if the initialization was successful, false otherwise
    ///
    virtual bool init(const int &idx) = 0;

    ///
    /// \brief start initializes the frame grabber loop and the camera
    ///              parameters if these have not been set upon the call of
    ///              init(). For devices like the Azure Kinect, these parameters
    ///              are read from the device's SDK and can be used as a
    ///              starting point for the optimization.
    /// \return      true, if successful, false otherwise
    ///
    virtual bool start() = 0;

    ///
    /// \brief pause simply pauses the frame grabber
    /// \return      true, if successful, false otherwise
    ///
    virtual bool pause() = 0;

    ///
    /// \brief stop halts the frame grabber loop
    /// \return     true, if successful, false otherwise
    ///
    virtual bool stop() = 0;

    ///
    /// \brief getInitialParameters returns the camera parameters
    /// \return
    ///
    virtual std::vector<Cam_Parameters> getInitialParameters() = 0;

    ///
    /// \brief isReady can be used to ask the camera class if the camera is
    ///                ready to capture images and the parameters are
    ///                initialized.
    /// \return
    ///
    virtual bool isReady() = 0;

    ///
    /// \brief getProperties is a getter for the property widgets controlling
    ///                      camera behaviour such as gain, exposure and white
    ///                      balance
    /// \return
    ///
    virtual std::vector<std::shared_ptr<Property>> getProperties() = 0;

    ///
    /// \brief supportsVisualization returns true if visualizeCalibration is
    ///                              implemented, i.e. if a device dependent
    ///                              visualization can be created
    /// \return
    ///
    virtual bool supportsVisualization() = 0;

    ///
    /// \brief visualizeCalibration can be used to provide a camera dependent
    ///                             visualization of the current parameters,
    ///                             e.g. a 3D pointcloud in the case of an Azure
    ///                             Kinect
    ///
    /// \param parameters           the parameter configuration to be used for
    ///                             the visualization
    /// \return                     true if successful, false otherwise
    ///
    virtual bool visualizeCalibration(const std::vector<Cam_Parameters>& parameters) = 0;

signals:
    ///
    /// \brief initialized          is emitted after the camera parameters have
    ///                             been set
    /// \param initial_parameters   is a vector of 1 or 2 Cam_Parameters structs
    ///                             depending on the number of cameras in the
    ///                             device (e.g. Azure Kinect has an RGB and an
    ///                             IR camera)
    ///
    void initialized(const std::vector<Cam_Parameters>& initial_parameters);

    ///
    /// \brief propertiesInitialized
    /// \param properties
    ///
    void propertiesInitialized(const std::vector<std::shared_ptr<Property>>& properties);

    ///
    /// \brief receivedNewFrame is emitted after the frame grabber successfully
    ///                         received a new frame
    /// \param frame            the new image (or RGB/IR image pair)
    ///
    void receivedNewFrame(std::shared_ptr<const Frame>& frame);

    ///
    /// \brief log  is emitted to send a message to the logger, but will mostly
    ///             be used via the macros STATUS(msg), WARN(msg) and ERROR(msg)
    ///             as defined in UI/logger.h
    /// \param msg  is the message
    /// \param type is the message type - check UI/logger.h
    ///
    void log(const QString& msg, const int& type);
};
