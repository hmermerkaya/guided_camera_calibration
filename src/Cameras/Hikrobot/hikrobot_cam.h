////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///     Implementation of the camera class for Hikrobot devices based on     ///
///                                 MVS SDK                                  ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Cameras/camera.h"

#include <MvCameraControl.h>

///
/// \brief The Hikrobot_Cam class
///
class Hikrobot_Cam : public Camera{
    Q_OBJECT

public:
    /// Destructor
    ~Hikrobot_Cam();

public slots:
    ///
    /// \brief getDeviceList is used to collect the device lists for different
    ///                      camera class implementations and give the user the
    ///                      opportunity to select among all available devices
    /// \return
    ///
    QStringList getDeviceList();

    ///
    /// \brief init checks if the selected device is available and initializes
    ///             it
    /// \param idx  is the index of the device in the list of devices
    /// \return     true, if the initialization was successful, false otherwise
    ///
    bool init(const int &idx);

    ///
    /// \brief start initializes the frame grabber loop and the camera
    ///              parameters via setInitialParams().
    /// \return      true, if successful, false otherwise
    ///
    bool start();

    ///
    /// \brief pause simply pauses the frame grabber by setting is_capturing_
    ///              to false
    /// \return      true, if successful, false otherwise
    ///
    bool pause();

    ///
    /// \brief stop halts the frame grabber loop
    /// \return     true, if successful, false otherwise
    ///
    bool stop();

    ///
    /// \brief getInitialParameters returns the camera parameters
    /// \return
    ///
    std::vector<Cam_Parameters> getInitialParameters();

    ///
    /// \brief isReady can be used to ask the camera class if the camera is
    ///                ready to capture images and the parameters are
    ///                initialized
    /// \return
    ///
    bool isReady(){return is_ready_;}

    ///
    /// \brief getProperties is a getter for the property widgets controlling
    ///                      camera behaviour such as gain, exposure and white
    ///                      balance
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getProperties(){return properties_;}

    ///
    /// \brief setPropertyState is used to set camera properties such as exposure,
    ///                    gain, white balance
    /// \param value
    /// \return
    ///
    bool setPropertyState(const QVariant& value);

    ///
    /// \brief supportsVisualization returns false since visualizeCalibration is
    ///                              currently not supported for Hikrobot cams
    /// \return
    ///
    bool supportsVisualization(){return false;}

    ///
    /// \brief visualizeCalibration is not implemented
    ///
    /// \param parameters           the parameter configuration to be used for
    ///                             the visualization
    /// \return                     true if successful, false otherwise
    ///
    bool visualizeCalibration(const std::vector<Cam_Parameters>& parameters);

private slots:

    ///
    /// \brief getNewImage is used to get a new k4a_capture_t and extract the
    ///                    the RGB and IR image from it. Finally it emits
    ///                    requestNewImage() basically calling itself in a
    ///                    Qt signal and slot-based capturing loop.
    ///
    void getNewImage();

    ///
    /// \brief setInitialParams reads the Kinect's parameters via the SDK and
    ///                         stores the relevant parts via Cam_Parameters
    ///                         structs
    ///
    void setInitialParams();

private:
    /// Camera handle
    void* handle_ = nullptr;
    unsigned char* data_ = nullptr;
    MVCC_INTVALUE param_;
    MV_CC_DEVICE_INFO* info_ = nullptr;
    unsigned int data_size_;

    ///The current RGB frame as QImage
    QImage current_rgb_frame_;

    /// The current RGB image as cv::Mat used for the purpose of conversion
    cv::Mat rgb_temp_;


    /// Camera parameters
    Cam_Parameters parameters_;

    /// A helper variable used to pause the capturing loop
    bool is_capturing_ = true;
    /// Contains the device status. Is set to true after initialization.
    bool is_ready_ = false;

    /// properties that can be modified by the user
    std::shared_ptr<Property_Spin_F> exposure_;
    std::shared_ptr<Property_Slider> brightness_;
    std::shared_ptr<Property_Slider> saturation_;
    std::shared_ptr<Property_Slider> sharpness_;
    std::shared_ptr<Property_Spin_F> gain_;
    std::shared_ptr<Property_Button> reset_to_auto_;

    std::vector<std::shared_ptr<Property>> properties_;

signals:

    ///
    /// \brief requestNewImage is emitted by getNewImage() after getting a frame
    ///                        to request the next one. Thus the capture loop is
    ///                        handled by Qt's signal/slot system which does not
    ///                        block all other functions of this class during
    ///                        capturing.
    ///
    void requestNewImage();
};
