////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///       Implementation of the camera class for Kinect v2 devices           ///
///       Note: The resolution of the Kinect v2 IR camera seems to           ///
///             be too low to work with the currently implemented            ///
///             ChArUco patterns. Accordingly, I excluded this               ///
///             class in the CMakeLists.txt                                  ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Cameras/camera.h"

#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/packet_pipeline.h>

///
/// \brief The Kinect_v2 class
///
class Kinect_v2 : public Camera{
    Q_OBJECT

public slots:
    ///
    /// \brief getDeviceList is used to collect the device lists for different
    ///                      camera class implementations and give the user the
    ///                      opportunity to select among all available devices
    /// \return
    ///
    QStringList getDeviceList();

    ///
    /// \brief init checks if the selected device is available and initializes
    ///             it
    /// \param idx  is the index of the device in the list of devices
    /// \return     true, if the initialization was successful, false otherwise
    ///
    bool init(const int &idx);

    ///
    /// \brief start initializes the frame grabber loop and the camera
    ///              parameters via setInitialParams().
    /// \return      true, if successful, false otherwise
    ///
    bool start();

    ///
    /// \brief pause simply pauses the frame grabber by setting is_capturing_
    ///              to false
    /// \return      true, if successful, false otherwise
    ///
    bool pause();

    ///
    /// \brief stop halts the frame grabber loop
    /// \return     true, if successful, false otherwise
    ///
    bool stop();

    ///
    /// \brief getInitialParameters returns the camera parameters
    /// \return
    ///
    std::vector<Cam_Parameters> getInitialParameters();

    ///
    /// \brief isReady can be used to ask the camera class if the camera is
    ///                ready to capture images and the parameters are
    ///                initialized.
    /// \return
    ///
    bool isReady(){return is_ready_;}

    ///
    /// \brief getProperties is a getter for the property widgets controlling
    ///                      camera behaviour such as gain, exposure and white
    ///                      balance
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getProperties(){return {};}

    ///
    /// \brief supportsVisualization returns false since visualizeCalibration is
    ///                              not implemented
    /// \return
    ///
    bool supportsVisualization(){return false;}

    ///
    /// \brief visualizeCalibration is not implemented for Kinect v2
    ///
    /// \param parameters           the parameter configuration to be used for
    ///                             the visualization
    /// \return                     true if successful, false otherwise
    ///
    bool visualizeCalibration(const std::vector<Cam_Parameters>& parameters){
        return false;
    }

private slots:

    ///
    /// \brief getNewImage is used to get a new frame from the
    ///                    libfreenect2::SyncMultiFrameListener and extract the
    ///                    RGB and IR images from it. Finally it emits
    ///                    requestNewImage() basically calling itself in a
    ///                    Qt signal and slot-based capturing loop.
    ///
    void getNewImage();

    ///
    /// \brief setInitialParams reads the Kinect's parameters via the SDK and
    ///                         stores the relevant parts via Cam_Parameters
    ///                         structs
    ///
    void setInitialParams();

private:

    /// Freenect2 instance
    libfreenect2::Freenect2 freenect2_;
    /// Current device's serial number
    std::string serial_;
    /// Pointer to the Kinect v2 device
    libfreenect2::Freenect2Device* dev_ = nullptr;
    /// Pipeline used to open the device
    libfreenect2::PacketPipeline* pipeline_ = nullptr;
    /// Frame listener
    libfreenect2::SyncMultiFrameListener* listener_ = nullptr;
    ///
    libfreenect2::FrameMap frames_;

    QImage current_rgb_frame_;
    QImage current_ir_frame_;

    ///
    std::vector<Cam_Parameters> parameters_ = std::vector<Cam_Parameters>(2);

    bool is_capturing_ = false;

    ///
    bool is_ready_ = false;

signals:

    ///
    /// \brief requestNewImage
    ///
    void requestNewImage();
};
