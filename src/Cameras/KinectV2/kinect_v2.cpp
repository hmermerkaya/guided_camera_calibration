#include "kinect_v2.h"
#include "UI/logger.h"

#include <QInputDialog>
#include <QThread>

QStringList Kinect_v2::getDeviceList(){
    QStringList camera_names;
    // Check whether devices are available
    int num_devices = freenect2_.enumerateDevices();
    if(num_devices == 0){
        WARN("No Kinect v2 devices detected.");
    }

    for(int i = 0; i < num_devices; i++){
        camera_names.push_back("Kinect v2 - " +
                               QString::fromStdString(freenect2_.getDeviceSerialNumber(i)));
    }

    return camera_names;
}

bool Kinect_v2::init(const int& idx){

    // Check whether devices are available
    if(freenect2_.enumerateDevices() == 0){
        ERROR("Kinect v2 not available.");
        return false;
    }

    // Get the serial of the selected device
    serial_ = freenect2_.getDeviceSerialNumber(idx);

    // Create the camera
    pipeline_ = new libfreenect2::CpuPacketPipeline();
    dev_ = freenect2_.openDevice(serial_, pipeline_);
    // Create a listener for the RGB and IR images
    listener_ = new libfreenect2::SyncMultiFrameListener(libfreenect2::Frame::Color | libfreenect2::Frame::Ir);
    dev_->setColorFrameListener(listener_);
    dev_->setIrAndDepthFrameListener(listener_);

    // Connect the image loop signal and slot
    connect(this, &Kinect_v2::requestNewImage, this, &Kinect_v2::getNewImage);

    return true;
}


bool Kinect_v2::start(){
    STATUS("Camera start requested..");
    if(dev_){
        is_capturing_ = true;
        dev_->start();
        setInitialParams();
    }
    return true;
}

bool Kinect_v2::pause(){
    // Toggle the is_capturing_ variable which is used in getNewImage() to
    // decide whether a frame will be requested
    is_capturing_ = !is_capturing_;
    if(!is_capturing_){
        STATUS("Camera pause requested..");
    }
    else{
        STATUS("Camera unpause requested..");
        emit requestNewImage();
    }
    return true;
}

bool Kinect_v2::stop(){
    STATUS("Camera stop requested..");
    if(dev_){
        dev_->stop();
    }
    return true;
}

std::vector<Cam_Parameters> Kinect_v2::getInitialParameters(){
    return parameters_;
}

void Kinect_v2::getNewImage(){

    // Capture frame - timeout at 1000ms
    if (!listener_->waitForNewFrame(frames_, 1000)){
        WARN("Skipped Kinect v2 frame.");

        if(is_capturing_){
            emit requestNewImage();
        }

        return;
    }
    libfreenect2::Frame* rgb = frames_[libfreenect2::Frame::Color];
    libfreenect2::Frame* ir = frames_[libfreenect2::Frame::Ir];

    // Cnvert RGB frame to QImage
    current_rgb_frame_ = QImage(rgb->data, parameters_[0].resolution_x,
            parameters_[0].resolution_y, QImage::Format_RGBA8888).mirrored(true, false);

    // Convert IR frame to cv::Mat first for color depth conversion
    cv::Mat ir_cv(ir->height, ir->width, CV_32FC1, ir->data);
    ir_cv.convertTo(ir_cv, CV_16U, 1.0, 0);
    // Convert to QImage
    current_ir_frame_ = QImage(ir_cv.data, parameters_[1].resolution_x,
            parameters_[1].resolution_y, QImage::Format_Grayscale16).mirrored(true, false);

    // Signal new images to other program parts
    emit receivedNewFrame(Frame(current_rgb_frame_, current_ir_frame_));

    // Force a maximum of 10 FPS to reduce CPU load
    QThread::msleep(100);

    // Release the frames and request the next image
    listener_->release(frames_);
    if(is_capturing_){
        emit requestNewImage();
    }
}

void Kinect_v2::setInitialParams(){

    is_ready_ = true;

    // Capture initial frame - timeout at 10s
    if (!listener_->waitForNewFrame(frames_, 10*1000)){
        ERROR("Unable to capture initial Kinect v2 frame.");
        return;
    }
    libfreenect2::Frame* rgb = frames_[libfreenect2::Frame::Color];
    libfreenect2::Frame* ir = frames_[libfreenect2::Frame::Ir];

    // Set resolutions
    parameters_[0].resolution_x = rgb->width;
    parameters_[0].resolution_y = rgb->height;
    parameters_[1].resolution_x = ir->width;
    parameters_[1].resolution_y = ir->height;

    listener_->release(frames_);

    // Get parameter structs from the devices
    libfreenect2::Freenect2Device::ColorCameraParams rgb_params =
            dev_->getColorCameraParams();
    libfreenect2::Freenect2Device::IrCameraParams ir_params =
            dev_->getIrCameraParams();

    // Set default initial rgb calibration parameters
    // Principal point = image center
    parameters_[0].cx_ = rgb_params.cx;
    parameters_[0].cy_ = rgb_params.cy;
    // Focal length
    parameters_[0].fx_ = rgb_params.fx;
    parameters_[0].fy_ = rgb_params.fy;
    // Distortion
    parameters_[0].k_[0] = 1.0;

    // Set default initial ir calibration parameters
    // Principal point = image center
    parameters_[1].cx_ = ir_params.cx;
    parameters_[1].cy_ = ir_params.cy;
    // Focal length
    parameters_[1].fx_ = ir_params.fx;
    parameters_[1].fy_ = ir_params.fy;
    // Distortion
    parameters_[1].k_[0] = 1.0;

    emit initialized(parameters_);

    // Start capturing
    emit requestNewImage();
}
