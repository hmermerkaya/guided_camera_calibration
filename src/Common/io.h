////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///     The IO header contains methods for file input/ouput                  ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/data_types.h"

#include <fstream>
#include <QFileDialog>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QPainter>
#include <QPdfWriter>

namespace IO{

///
/// \brief createPatternPDF creates a PDF file at a user chosen location
///                         containing the pattern image
/// \param pattern          image of the calibration pattern
/// \param page_size        page format, such as A3, A4, etc.
/// \param dpi              PDF printing resolution
///
inline void createPatternPDF(const QImage& pattern_image,
                             const QPageSize& page_size,
                             const float& dpi){

    // Ask the user where to store the file
    QString file_path = QFileDialog::getSaveFileName(nullptr, "Create a new PDF");
    if(file_path.right(3).compare("pdf") != 0){
        file_path += ".pdf";
    }

    // Set PDF properties
    QPdfWriter pdf_writer(file_path);
    pdf_writer.setPageSize(page_size);
    pdf_writer.setResolution(int(dpi));
    if(pattern_image.width() > pattern_image.height()){
        pdf_writer.setPageOrientation(QPageLayout::Landscape);
    }

    // Find scaling factor for pattern image
    float pdf_height = float(pdf_writer.height());
    float pdf_width = float(pdf_writer.width());
    int scaling = int( std::min(pdf_height / float(pattern_image.height()),
                                pdf_width / float(pattern_image.width())));
    int offset_width = (pdf_writer.width() - scaling * pattern_image.width())/2;
    int offset_height = (pdf_writer.height() - scaling * pattern_image.height())/2;

    // Create painter to draw pattern_image into pdf
    QPainter painter(&pdf_writer);
    painter.drawImage(offset_width, offset_height,
                      pattern_image.scaled(scaling * pattern_image.width(),
                                           scaling * pattern_image.height()));

}

///
/// \brief saveCameraParameters
/// \param params
/// \return
///
inline bool saveCameraParameters(const std::vector<Cam_Parameters>& params, std::string* error){
    // Ask the user where to store the file
    QString file_path = QFileDialog::getSaveFileName(nullptr,
                                                     "Create a new calibration file",
                                                     QDir::homePath(),
                                                     "Text Files (*.txt);JSON Files (*.json)",
                                                     nullptr,
                                                     QFileDialog::DontConfirmOverwrite);

    if(file_path.size() == 0){
        *error = "No file chosen.";
        return false;
    }

    bool is_json = !(file_path.right(8).compare("MCC_Cams") == 0);

    if(is_json && file_path.right(4).compare("json") != 0){
        file_path += ".json";
    }

    if(is_json){
        QFile file(file_path);
        if(!file.open(QIODevice::ReadWrite)) {
            *error = "Unable to open the file";
            return false;
        }

        // Check whether original file already contains calibration data
        QString val = file.readAll();
        QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
        QJsonArray json_data = doc.array();

        if(json_data.size() > 0){
            if(QMessageBox::question(nullptr, "Question",
                                     "The file already contains camera data. Do you want to keep the data?",
                                     QMessageBox::Yes | QMessageBox::No) == QMessageBox::No){
                json_data = QJsonArray();
            }
        }

        for(const Cam_Parameters& cam : params) {
            QJsonObject cam_object;
            cam_object.insert("Name", QString::fromStdString(cam.camera_name_));
            QJsonObject cam_params_object;

            QJsonObject res_object;
            res_object.insert("Width",cam.resolution_x);
            res_object.insert("Height",cam.resolution_y);
            cam_params_object.insert("Resolution", res_object);

            QJsonObject K_object;
            K_object.insert("fx",cam.fx_);
            K_object.insert("fy",cam.fy_);
            K_object.insert("cx",cam.cx_);
            K_object.insert("cy",cam.cy_);
            cam_params_object.insert("K matrix", K_object);

            QJsonObject distortion_object;
            distortion_object.insert("Distortion model", cam.distortion_model_);
            distortion_object.insert("k1",cam.k_[0]);
            distortion_object.insert("k2",cam.k_[1]);
            distortion_object.insert("k3",cam.k_[2]);
            distortion_object.insert("k4",cam.k_[3]);
            distortion_object.insert("k5",cam.k_[4]);
            distortion_object.insert("k6",cam.k_[5]);
            distortion_object.insert("p1",cam.p1_);
            distortion_object.insert("p2",cam.p2_);
            cam_params_object.insert("Distortion parameters", distortion_object);

            QJsonObject pose_object;
            QJsonArray rotation;
            rotation.append(cam.R_rod_[0]);
            rotation.append(cam.R_rod_[1]);
            rotation.append(cam.R_rod_[2]);
            pose_object.insert("Rotation (Rodrigues Vector)", rotation);
            QJsonArray translation;
            translation.append(cam.t_[0]);
            translation.append(cam.t_[1]);
            translation.append(cam.t_[2]);
            pose_object.insert("Translation", translation);
            cam_params_object.insert("Pose (Transformation to reference camera)", pose_object);

            cam_object.insert("Parameters", cam_params_object);

            // Check whether camera is already available in old json data
            bool replaced = false;
            for(int cam_idx = 0; cam_idx < json_data.count(); cam_idx++){
                QJsonObject cam_obj = json_data.at(cam_idx).toObject();
                QJsonValue name = cam_obj.value(QString("Name"));
                std::string camera_name = name.toString().toStdString();
                if(camera_name.compare(cam.camera_name_) == 0){
                    json_data.insert(cam_idx, cam_object);
                    json_data.removeAt(cam_idx+1);
                    replaced = true;
                }
            }
            if(!replaced){
                json_data.append(cam_object);
            }
        }

        // Clear the original content in the file
        file.resize(0);
        //QJsonDocument doc;
        doc.setArray(json_data);
        file.write(doc.toJson());
        file.close();
    }
    else{
        // open target file
        std::ofstream file(file_path.toStdString());
        if(!file.is_open()){
            *error = "Unable to open the file";
            return false;
        }

        // write header
        file << "$CamID $Width $Height $CamType\n"
                "$distortion.k_1 $distortion.k_2 $distortion.p_1 $distortion.p_2 "
                "$distortion.k_3 $distortion.k_4 $distortion.k_5 $distortion.k_6\n"
                "$K matrix rowwise\n"
                "$Pose matrix rowwise\n\n";

        // write device parameters
        for(size_t i = 0; i < params.size(); i++){

            const Cam_Parameters& cam = params[i];

            // write camera name and resolution
            file << "Cam " << cam.camera_name_ << " " << cam.resolution_x
                 << " " << cam.resolution_y << std::endl;

            // write distortion parameters
            file << cam.k_[0] << " " << cam.k_[1] << " " << cam.p1_ << " "
                              << cam.p2_ << " " << cam.k_[2] << " " << cam.k_[3]
                              << " " << cam.k_[4] << " " << cam.k_[5] << std::endl;

            // write K matrix
            file << cam.fx_ << " " << 0.0 << " " << cam.cx_
                 << 0.0 << " " << cam.fy_ << " " << cam.cy_
                 << 0.0 << " " << 0.0 << " " << 1.0 << std::endl;

            // write pose
            Pose pose(cam.R_rod_, cam.t_);
            for(size_t row = 0; row < 4; row++){
                for(size_t col = 0; col < 4; col++){
                    if(row != 3 || col != 3){
                        file << pose.mat_(row,col) << " ";
                    }
                }
            }
            file << pose.mat_(3,3) << std::endl;
        }

        file.close();
    }

    return true;


}

///
/// \brief loadCameraParameters
/// \param params
/// \return
///
inline bool loadCameraParameters(std::vector<Cam_Parameters>* params, std::string* error){

    // Check if pointers valid
    if(!params || !error){
        return false;
    }

    // Ask the user which file to load
    QString file_path = QFileDialog::getOpenFileName(nullptr,
                                                     "Select a calibration file");

    if(file_path.size() == 0){
        *error = "No file chosen.";
        return false;
    }

    // Open file and read all data
    QString val;
    QFile file;
    file.setFileName(file_path);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();

    // Decode json structure
    QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
    QJsonArray root = doc.array();
    for(int cam_idx = 0; cam_idx < root.count(); cam_idx++){
        QJsonValue cam_obj_val = root.at(cam_idx);
        QJsonObject cam_obj = cam_obj_val.toObject();
        QJsonObject params_obj = cam_obj.value("Parameters").toObject();

        // Camera name
        QJsonValue name = cam_obj.value(QString("Name"));
        std::string camera_name = name.toString().toStdString();

        // Create new device
        Cam_Parameters loaded_params;
        loaded_params.camera_name_ = camera_name;

        // Resolution
        QJsonObject res_obj = params_obj.value(QString("Resolution")).toObject();
        loaded_params.resolution_x = res_obj["Width"].toInt();
        loaded_params.resolution_y = res_obj["Height"].toInt();

        // K matrix
        QJsonObject K_obj = params_obj.value(QString("K matrix")).toObject();
        loaded_params.fx_ = K_obj["fx"].toDouble();
        loaded_params.fy_ = K_obj["fy"].toDouble();
        loaded_params.cx_ = K_obj["cx"].toDouble();
        loaded_params.cy_ = K_obj["cy"].toDouble();

        // Distortion parameters
        QJsonObject dist_obj = params_obj.value(QString("Distortion parameters")).toObject();
        loaded_params.p1_ = dist_obj["p1"].toDouble();
        loaded_params.p2_ = dist_obj["p2"].toDouble();
        loaded_params.k_.clear();
        loaded_params.k_.push_back(dist_obj["k1"].toDouble());
        loaded_params.k_.push_back(dist_obj["k2"].toDouble());
        loaded_params.k_.push_back(dist_obj["k3"].toDouble());
        loaded_params.k_.push_back(dist_obj["k4"].toDouble());
        loaded_params.k_.push_back(dist_obj["k5"].toDouble());
        loaded_params.k_.push_back(dist_obj["k6"].toDouble());
        loaded_params.distortion_model_ = dist_obj["Distortion model"].toInt();

        QJsonObject pose_object = params_obj.value(QString("Pose (Transformation to reference camera)")).toObject();
        QJsonArray rotation = pose_object.value(QString("Rotation (Rodrigues Vector)")).toArray();
        loaded_params.R_rod_[0] = rotation.at(0).toDouble();
        loaded_params.R_rod_[1] = rotation.at(1).toDouble();
        loaded_params.R_rod_[2] = rotation.at(2).toDouble();
        Eigen::AngleAxisd aa = Eigen::AngleAxisd(loaded_params.R_rod_.norm(), loaded_params.R_rod_/loaded_params.R_rod_.norm());
        loaded_params.R_ = aa.toRotationMatrix();

        QJsonArray translation = pose_object.value(QString("Translation")).toArray();
        loaded_params.t_[0] = translation.at(0).toDouble();
        loaded_params.t_[1] = translation.at(1).toDouble();
        loaded_params.t_[2] = translation.at(2).toDouble();

        params->push_back(loaded_params);
    }

    return true;
}

///
/// \brief saveUISettings
/// \param settings
/// \param error
/// \return
///
inline bool saveUISettings(const std::vector<std::shared_ptr<Property>>& settings, const bool& ask_user, std::string* error){

    if(!error || settings.size() == 0){
        return false;
    }

    QString file_path = "default_settings.json";
    if(ask_user){
        // Ask the user where to store the file
        file_path = QFileDialog::getSaveFileName(nullptr,
                                                 "Create a new settings file",
                                                 QDir::homePath(),
                                                 "JSON Files (*.json)",
                                                 nullptr,
                                                 QFileDialog::DontConfirmOverwrite);

        if(file_path.size() == 0){
            *error = "No file chosen.";
            return false;
        }

        if(file_path.right(4).compare("json") != 0){
            file_path += ".json";
        }
    }

    // Open file
    QFile file(file_path);
    if(!file.open(QIODevice::ReadWrite)) {
        *error = "Unable to open the file";
        return false;
    }

    QJsonArray json_data;
    for(const std::shared_ptr<Property>& setting : settings) {
        QJsonObject setting_object;
        setting_object.insert("Name", setting->name());
        int type = setting->type();
        setting_object.insert("Type", type);
        if(type == PROP_SLIDER_INT){
            Property_Slider* slider = static_cast<Property_Slider*>(setting.get());
            setting_object.insert("Value", slider->value());
        }
        else if(type == PROP_SPIN_INT){
            Property_Spin* spin = static_cast<Property_Spin*>(setting.get());
            setting_object.insert("Value", spin->value());
        }
        else if(type == PROP_SPIN_DOUBLE){
            Property_Spin_F* spin = static_cast<Property_Spin_F*>(setting.get());
            setting_object.insert("Value", spin->value());
        }
        else if(type == PROP_SELECTOR){
            Property_Selector* select = static_cast<Property_Selector*>(setting.get());
            setting_object.insert("Value", select->index());
        }
        else if(type == PROP_CHECK){
            Property_Check* check = static_cast<Property_Check*>(setting.get());
            setting_object.insert("Value", check->value());
        }
        json_data.append(setting_object);
    }

    // Clear the original content in the file
    file.resize(0);
    //QJsonDocument doc;
    QJsonDocument doc;
    doc.setArray(json_data);
    file.write(doc.toJson());
    file.close();

    return true;
}

///
/// \brief loadUISettings
/// \param settings
/// \param ask_user
/// \param error
/// \return
///
inline bool loadUISettings(const std::vector<std::shared_ptr<Property>>& settings, const bool& ask_user, std::string* error){

    if(!error || settings.size() == 0){
        return false;
    }

    QString file_path = "default_settings.json";
    if(ask_user){
        // Ask the user which file to load
        file_path = QFileDialog::getOpenFileName(nullptr,
                                                 "Load settings file",
                                                 QDir::homePath(),
                                                 "JSON Files (*.json)",
                                                 nullptr);

        if(file_path.size() == 0){
            *error = "No file chosen.";
            return false;
        }

        if(file_path.right(4).compare("json") != 0){
            file_path += ".json";
        }
    }

    // Open file and read all data
    QString val;
    QFile file;
    file.setFileName(file_path);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)){
        val = file.readAll();
        file.close();

        // Decode json structure
        QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
        QJsonArray root = doc.array();
        for(int property_idx = 0; property_idx < root.count(); property_idx++){
            QJsonValue property_val = root.at(property_idx);
            QJsonObject property_obj = property_val.toObject();
            QString name = property_obj.value(QString("Name")).toString();
            int type = property_obj.value(QString("Type")).toInt();
            QJsonValue value = property_obj.value(QString("Value"));
            QVariant value_var(value);
            //
            for(std::shared_ptr<Property> property : settings){
                if(property->name().compare(name) == 0){
                    property->setValue(value_var);
                    break;
                }
            }

        }

        return true;
    }
    return false;
}

} // namespace IO
