////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///   The data class holds pointers to the different non-GUI program parts   ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Cameras/camera.h"
#include "Optimization/optimizer.h"
#include "Patterns/pattern.h"
#include "UI/ar_overlay.h"

#include <QObject>
#include <QThread>

///
/// \brief The Data class
///
class Data : public QObject{

    Q_OBJECT

public:

    ///
    /// \brief Data is the default constructor
    ///
    Data();

    /// Destructor
    ~Data();

public:

    /// Pointer to pattern creator/detector instance
    std::shared_ptr<Pattern> pattern_;
    /// Corresponding QThread
    std::unique_ptr<QThread> pattern_thread_;

    /// Pointer to camera device instance
    /// TODO: for some reason the camera feed does not work when using smart
    ///       pointers
    Camera* camera_;
    /// Corresponding QThread
    std::unique_ptr<QThread> camera_thread_;

    /// Pointer to the AR Overlay instance which calculates the overlay position
    std::unique_ptr<AR_Overlay> ar_overlay_;
    /// Corresponding QThread
    std::unique_ptr<QThread> ar_overlay_thread_;

    /// Pointer to the optimizer instance
    std::shared_ptr<Optimizer> optimizer_;
    /// Corresponding QThread
    std::unique_ptr<QThread> optimizer_thread_;

public slots:

    ///
    /// \brief switchPatternType
    /// \param type
    /// \return
    ///
    bool switchPatternType(const int& type);

    ///
    /// \brief getAllProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getAllProperties();

signals:

    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(const QString& msg, const int& type);
};


