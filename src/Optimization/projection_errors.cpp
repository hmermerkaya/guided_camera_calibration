#include "projection_errors.h"

template <typename T>
bool Projection_Error_Intrinsics_Brown::operator()(const T* const K, const T* const dist,
                                                   const T* const rot_P, const T* const t_P,
                                                   T* residuals) const{

    // rotate pattern point into world coordinates
    T P_pattern[3] = {T(P_[0]), T(P_[1]), T(P_[2])};
    T P_world[3];
    ceres::AngleAxisRotatePoint(rot_P, P_pattern, P_world);

    // translate point
    P_world[0] += t_P[0];
    P_world[1] += t_P[1];
    P_world[2] += t_P[2];

    // transform point into target camera - note: R_rod_, t_ describe the trans-
    // formation from the current camera into the world frame and have to be
    // inverted

    // translate point
    P_world[0] -= T(t_[0]);
    P_world[1] -= T(t_[1]);
    P_world[2] -= T(t_[2]);

    // rotate
    T P_cam[3];// = {P_world[0],P_world[1],P_world[2]};
    T R_rod[3] = {T(-R_rod_[0]), T(-R_rod_[1]), T(-R_rod_[2])};
    ceres::AngleAxisRotatePoint(R_rod, P_world, P_cam);

    // homogenize
    P_cam[0] /= P_cam[2];
    P_cam[1] /= P_cam[2];

    // apply distortion
    T r2 = P_cam[0]*P_cam[0] + P_cam[1]*P_cam[1];
    T r4 = r2 * r2;
    T r6 = r4 * r2;
    T x2 = P_cam[0] * P_cam[0];
    T y2 = P_cam[1] * P_cam[1];
    T xy = P_cam[0] * P_cam[1];

    T dist_rad = (1.0 + dist[2]*r2 + dist[3]*r4 + dist[4]*r6);

    T x_dist = P_cam[0] * dist_rad + 2.0 * dist[0] * xy + dist[1] * (r2 + 2.0 * x2);
    T y_dist = P_cam[1] * dist_rad + 2.0 * dist[1] * xy + dist[0] * (r2 + 2.0 * y2);

    // transfer from camera to pixel coordinates
    T x_projected = (K[0] * x_dist) + K[2];
    T y_projected = (K[1] * y_dist) + K[3];

    // calculate residuals, note: ceres squares these automatically
    residuals[0] = T(weight_) * (x_projected - T(p_[0]));
    residuals[1] = T(weight_) * (y_projected - T(p_[1]));

    return true;
}


template <typename T>
bool Projection_Error_Intrinsics_Rational::operator()(const T* const K, const T* const dist,
                                                      const T* const rot_P, const T* const t_P,
                                                      T* residuals) const{

    // rotate pattern point into world coordinates
    T P_pattern[3] = {T(P_[0]), T(P_[1]), T(P_[2])};
    T P_world[3];
    ceres::AngleAxisRotatePoint(rot_P, P_pattern, P_world);

    // translate point
    P_world[0] += t_P[0];
    P_world[1] += t_P[1];
    P_world[2] += t_P[2];

    // transform point into target camera - note: R_rod_, t_ describe the trans-
    // formation from the current camera into the world frame and have to be
    // inverted

    // translate point
    P_world[0] -= T(t_[0]);
    P_world[1] -= T(t_[1]);
    P_world[2] -= T(t_[2]);

    // rotate
    T P_cam[3];// = {P_world[0],P_world[1],P_world[2]};
    T R_rod[3] = {T(-R_rod_[0]), T(-R_rod_[1]), T(-R_rod_[2])};
    ceres::AngleAxisRotatePoint(R_rod, P_world, P_cam);

    // homogenize
    P_cam[0] /= P_cam[2];
    P_cam[1] /= P_cam[2];

    // apply distortion
    T r2 = P_cam[0]*P_cam[0] + P_cam[1]*P_cam[1];
    T r4 = r2 * r2;
    T r6 = r4 * r2;
    T x2 = P_cam[0] * P_cam[0];
    T y2 = P_cam[1] * P_cam[1];
    T xy = P_cam[0] * P_cam[1];

    T dist_rad = (1.0 + dist[2]*r2 + dist[3]*r4 + dist[4]*r6) /
            (1.0 + dist[5]*r2 + dist[6]*r4 + dist[7]*r6);

    T x_dist = P_cam[0] * dist_rad + 2.0 * dist[0] * xy + dist[1] * (r2 + 2.0 * x2);
    T y_dist = P_cam[1] * dist_rad + 2.0 * dist[1] * xy + dist[0] * (r2 + 2.0 * y2);

    // transfer from camera to pixel coordinates
    T x_projected = (K[0] * x_dist) + K[2];
    T y_projected = (K[1] * y_dist) + K[3];

    // calculate residuals, note: ceres squares these automatically
    residuals[0] = T(weight_) * (x_projected - T(p_[0]));
    residuals[1] = T(weight_) * (y_projected - T(p_[1]));

    return true;
}


template <typename T>
bool Projection_Error_Extrinsics_Brown::operator()(const T* const R_rod_cam,
                                                   const T* const t_cam,
                                                   const T* const R_rod_P,
                                                   const T* const t_P,
                                                   T* residuals) const{
    // rotate pattern point into world coordinates
    T P_pattern[3] = {T(P_[0]), T(P_[1]), T(P_[2])};
    T P_world[3];
    ceres::AngleAxisRotatePoint(R_rod_P, P_pattern, P_world);

    // translate point
    P_world[0] += t_P[0];
    P_world[1] += t_P[1];
    P_world[2] += t_P[2];

    //////////////////////// project point to first camera /////////////////////

    // homogenize
    T P_cam_0[2];
    P_cam_0[0] = P_world[0]/P_world[2];
    P_cam_0[1] = P_world[1]/P_world[2];

    // get distortion parameters for camera 1
    int cam_idx = first_cam_is_ref_ ? 0 : 1;
    T fx = T(params_[cam_idx].fx_);
    T fy = T(params_[cam_idx].fy_);
    T cx = T(params_[cam_idx].cx_);
    T cy = T(params_[cam_idx].cy_);
    T k[3] = {T(params_[cam_idx].k_[0]), T(params_[cam_idx].k_[1]),
              T(params_[cam_idx].k_[2])};
    T p1 = T(params_[cam_idx].p1_);
    T p2 = T(params_[cam_idx].p2_);

    // apply distortion
    T x2 = P_cam_0[0] * P_cam_0[0];
    T y2 = P_cam_0[1] * P_cam_0[1];
    T xy = P_cam_0[0] * P_cam_0[1];
    T r2 = x2 + y2;
    T r4 = r2 * r2;
    T r6 = r4 * r2;

    T dist_rad = (1.0 + k[0]*r2 + k[1]*r4 + k[2]*r6);

    T x_dist = P_cam_0[0] * dist_rad + 2.0 * p1 * xy + p2 * (r2 + 2.0 * x2);
    T y_dist = P_cam_0[1] * dist_rad + 2.0 * p2 * xy + p1 * (r2 + 2.0 * y2);

    // transfer from camera to pixel coordinates
    T x_projected = fx * x_dist + cx;
    T y_projected = fy * y_dist + cy;

    // calculate residuals, note: ceres squares these automatically
    residuals[0] = T(weight_) * (x_projected - T(p_0_[0]));
    residuals[1] = T(weight_) * (y_projected - T(p_0_[1]));

    //////////////////////// project point to second camera ////////////////////

    // transform world point into camera 2 coordinate frame
    T P_cam_1[3];
    if(first_cam_is_ref_){
        // translate point
        P_world[0] -= t_cam[0];
        P_world[1] -= t_cam[1];
        P_world[2] -= t_cam[2];

        // rotate point
        T R_rod[3] = {-R_rod_cam[0],-R_rod_cam[1],-R_rod_cam[2]};
        ceres::AngleAxisRotatePoint(R_rod, P_world, P_cam_1);
    }
    else{
        // rotate pattern point into world coordinates
        T R_rod[3] = {R_rod_cam[0], R_rod_cam[1], R_rod_cam[2]};
        ceres::AngleAxisRotatePoint(R_rod, P_world, P_cam_1);

        // translate point
        P_cam_1[0] += t_cam[0];
        P_cam_1[1] += t_cam[1];
        P_cam_1[2] += t_cam[2];
    }

    // homogenize
    P_cam_1[0] /= P_cam_1[2];
    P_cam_1[1] /= P_cam_1[2];


    // get distortion parameters for camera 2
    cam_idx = 1 - cam_idx;
    fx = T(params_[cam_idx].fx_);
    fy = T(params_[cam_idx].fy_);
    cx = T(params_[cam_idx].cx_);
    cy = T(params_[cam_idx].cy_);
    T k1[3] = {T(params_[cam_idx].k_[0]), T(params_[cam_idx].k_[1]), T(params_[cam_idx].k_[2])};
    p1 = T(params_[cam_idx].p1_);
    p2 = T(params_[cam_idx].p2_);

    // apply distortion
    x2 = P_cam_1[0] * P_cam_1[0];
    y2 = P_cam_1[1] * P_cam_1[1];
    xy = P_cam_1[0] * P_cam_1[1];
    r2 = x2 + y2;
    r4 = r2 * r2;
    r6 = r4 * r2;

    dist_rad = (1.0 + k1[0]*r2 + k1[1]*r4 + k1[2]*r6);

    x_dist = P_cam_1[0] * dist_rad + 2.0 * p1 * xy + p2 * (r2 + 2.0 * x2);
    y_dist = P_cam_1[1] * dist_rad + 2.0 * p2 * xy + p1 * (r2 + 2.0 * y2);

    // transfer from camera to pixel coordinates
    x_projected = fx * x_dist + cx;
    y_projected = fy * y_dist + cy;

    // calculate residuals, note: ceres squares these automatically
    residuals[2] = T(weight_) * (x_projected - T(p_1_[0]));
    residuals[3] = T(weight_) * (y_projected - T(p_1_[1]));

    return true;
}

std::vector<double> Projection_Error_Extrinsics_Brown::evaluate(const double* R_rod_cam,
                                                                const double* t_cam,
                                                                const double* R_rod_P,
                                                                const double* t_P) const{
    std::vector<double> residuals = std::vector<double>(4, 0.0);
    this->operator ()(R_rod_cam, t_cam, R_rod_P, t_P, residuals.data());
    return residuals;
}

template <typename T>
bool Projection_Error_Extrinsics_Rational::operator()(const T* const R_rod_cam,
                                                      const T* const t_cam,
                                                      const T* const R_rod_P,
                                                      const T* const t_P,
                                                      T* residuals) const{
    // rotate pattern point into world coordinates
    T P_pattern[3] = {T(P_[0]), T(P_[1]), T(P_[2])};
    T P_world[3];
    ceres::AngleAxisRotatePoint(R_rod_P, P_pattern, P_world);

    // translate point
    P_world[0] += t_P[0];
    P_world[1] += t_P[1];
    P_world[2] += t_P[2];

    //////////////////////// project point to first camera /////////////////////

    // homogenize
    T P_cam_0[2];
    P_cam_0[0] = P_world[0]/P_world[2];
    P_cam_0[1] = P_world[1]/P_world[2];

    // get distortion parameters for camera 1
    int cam_idx = first_cam_is_ref_ ? 0 : 1;
    T fx = T(params_[cam_idx].fx_);
    T fy = T(params_[cam_idx].fy_);
    T cx = T(params_[cam_idx].cx_);
    T cy = T(params_[cam_idx].cy_);
    T k[6] = {T(params_[cam_idx].k_[0]), T(params_[cam_idx].k_[1]), T(params_[cam_idx].k_[2]),
              T(params_[cam_idx].k_[3]), T(params_[cam_idx].k_[4]), T(params_[cam_idx].k_[5])};
    T p1 = T(params_[cam_idx].p1_);
    T p2 = T(params_[cam_idx].p2_);

    // apply distortion
    T x2 = P_cam_0[0] * P_cam_0[0];
    T y2 = P_cam_0[1] * P_cam_0[1];
    T xy = P_cam_0[0] * P_cam_0[1];
    T r2 = x2 + y2;
    T r4 = r2 * r2;
    T r6 = r4 * r2;

    T dist_rad = (1.0 + k[0]*r2 + k[1]*r4 + k[2]*r6) /
            (1.0 + k[3]*r2 + k[4]*r4 + k[5]*r6);


    T x_dist = P_cam_0[0] * dist_rad + 2.0 * p1 * xy + p2 * (r2 + 2.0 * x2);
    T y_dist = P_cam_0[1] * dist_rad + 2.0 * p2 * xy + p1 * (r2 + 2.0 * y2);

    // transfer from camera to pixel coordinates
    T x_projected = fx * x_dist + cx;
    T y_projected = fy * y_dist + cy;

    // calculate residuals, note: ceres squares these automatically
    residuals[0] = T(weight_) * (x_projected - T(p_0_[0]));
    residuals[1] = T(weight_) * (y_projected - T(p_0_[1]));

    //////////////////////// project point to second camera ////////////////////

    // transform world point into camera 2 coordinate frame
    T P_cam_1[3];
    if(first_cam_is_ref_){
        // translate point
        P_world[0] -= t_cam[0];
        P_world[1] -= t_cam[1];
        P_world[2] -= t_cam[2];

        // rotate point
        T R_rod[3] = {-R_rod_cam[0],-R_rod_cam[1],-R_rod_cam[2]};
        ceres::AngleAxisRotatePoint(R_rod, P_world, P_cam_1);
    }
    else{
        // rotate pattern point into world coordinates
        T R_rod[3] = {R_rod_cam[0], R_rod_cam[1], R_rod_cam[2]};
        ceres::AngleAxisRotatePoint(R_rod, P_world, P_cam_1);

        // translate point
        P_cam_1[0] += t_cam[0];
        P_cam_1[1] += t_cam[1];
        P_cam_1[2] += t_cam[2];
    }

    // homogenize
    P_cam_1[0] /= P_cam_1[2];
    P_cam_1[1] /= P_cam_1[2];


    // get distortion parameters for camera 2
    cam_idx = 1 - cam_idx;
    fx = T(params_[cam_idx].fx_);
    fy = T(params_[cam_idx].fy_);
    cx = T(params_[cam_idx].cx_);
    cy = T(params_[cam_idx].cy_);
    T k1[6] = {T(params_[cam_idx].k_[0]), T(params_[cam_idx].k_[1]), T(params_[cam_idx].k_[2]),
               T(params_[cam_idx].k_[3]), T(params_[cam_idx].k_[4]), T(params_[cam_idx].k_[5])};
    p1 = T(params_[cam_idx].p1_);
    p2 = T(params_[cam_idx].p2_);

    // apply distortion
    x2 = P_cam_1[0] * P_cam_1[0];
    y2 = P_cam_1[1] * P_cam_1[1];
    xy = P_cam_1[0] * P_cam_1[1];
    r2 = x2 + y2;
    r4 = r2 * r2;
    r6 = r4 * r2;

    dist_rad = (1.0 + k1[0]*r2 + k1[1]*r4 + k1[2]*r6) /
            (1.0 + k[3]*r2 + k[4]*r4 + k[5]*r6);

    x_dist = P_cam_1[0] * dist_rad + 2.0 * p1 * xy + p2 * (r2 + 2.0 * x2);
    y_dist = P_cam_1[1] * dist_rad + 2.0 * p2 * xy + p1 * (r2 + 2.0 * y2);

    // transfer from camera to pixel coordinates
    x_projected = fx * x_dist + cx;
    y_projected = fy * y_dist + cy;

    // calculate residuals, note: ceres squares these automatically
    residuals[2] = T(weight_) * (x_projected - T(p_1_[0]));
    residuals[3] = T(weight_) * (y_projected - T(p_1_[1]));

    return true;
}

std::vector<double> Projection_Error_Extrinsics_Rational::evaluate(const double* R_rod_cam,
                                                                   const double* t_cam,
                                                                   const double* R_rod_P,
                                                                   const double* t_P) const{
    std::vector<double> residuals = std::vector<double>(4, 0.0);
    this->operator ()(R_rod_cam, t_cam, R_rod_P, t_P, residuals.data());
    return residuals;
}

template <typename T>
bool Projection_Error_All::operator()(const T* const K_1, const T* const dist_1,
                                      const T* const K_2, const T* const dist_2,
                                      const T* const R_rod_cam, const T* const t_cam,
                                      const T* const R_rod_P, const T* const t_P,
                                      T* residuals) const{

    // rotate pattern point into world coordinates
    T P_pattern[3] = {T(P_[0]), T(P_[1]), T(P_[2])};
    T P_world[3];
    ceres::AngleAxisRotatePoint(R_rod_P, P_pattern, P_world);

    // translate point
    P_world[0] += t_P[0];
    P_world[1] += t_P[1];
    P_world[2] += t_P[2];

    ///////////////////////   Camera 1 projection error   //////////////////////

    // homogenize
    T P_cam_1[2];
    P_cam_1[0] /= P_world[0]/P_world[2];
    P_cam_1[1] /= P_world[1]/P_world[2];

    // apply distortion
    T x2 = P_cam_1[0] * P_cam_1[0];
    T y2 = P_cam_1[1] * P_cam_1[1];
    T r2 = x2 + y2;
    T r4 = r2 * r2;
    T r6 = r4 * r2;
    T xy = P_cam_1[0] * P_cam_1[1];

    T dist_rad = (1.0 + dist_1[0]*r2 + dist_1[1]*r4 + dist_1[2]*r6)/
            (1.0 + dist_1[3]*r2 + dist_1[4]*r4 + dist_1[5]*r6);

    T x_dist = P_cam_1[0] * dist_rad + 2.0 * dist_1[6] * xy + dist_1[7] * (r2 + 2.0 * x2);
    T y_dist = P_cam_1[1] * dist_rad + 2.0 * dist_1[7] * xy + dist_1[6] * (r2 + 2.0 * y2);

    // transfer from camera to pixel coordinates
    T x_projected = (K_1[0] * x_dist) + K_1[2];
    T y_projected = (K_1[1] * y_dist) + K_1[3];

    // calculate residuals, note: ceres squares these automatically
    residuals[0] = T(weight_) * (x_projected - T(p_0_[0]));
    residuals[1] = T(weight_) * (y_projected - T(p_0_[1]));

    ///////////////////////   Camera 2 projection error   //////////////////////

    // transform point into target camera - note: R_rod_, t_ describe the trans-
    // formation from the current camera into the world frame and have to be
    // inverted

    // translate point
    P_world[0] -= t_cam[0];
    P_world[1] -= t_cam[1];
    P_world[2] -= t_cam[2];

    // rotate
    T P_cam_2[3];
    T R_rod[3] = {-R_rod_cam[0], -R_rod_cam[1], -R_rod_cam[2]};
    ceres::AngleAxisRotatePoint(R_rod, P_world, P_cam_2);

    // homogenize
    P_cam_2[0] / P_cam_2[2];
    P_cam_2[1] / P_cam_2[2];

    // apply distortion
    x2 = P_cam_2[0] * P_cam_2[0];
    y2 = P_cam_2[1] * P_cam_2[1];
    r2 = x2 + y2;
    r4 = r2 * r2;
    r6 = r4 * r2;
    xy = P_cam_2[0] * P_cam_2[1];

    dist_rad = (1.0 + dist_2[0]*r2 + dist_2[1]*r4 + dist_2[2]*r6)/
            (1.0 + dist_2[3]*r2 + dist_2[4]*r4 + dist_2[5]*r6);

    x_dist = P_cam_2[0] * dist_rad + 2.0 * dist_2[6] * xy + dist_2[7] * (r2 + 2.0 * x2);
    y_dist = P_cam_2[1] * dist_rad + 2.0 * dist_2[7] * xy + dist_2[6] * (r2 + 2.0 * y2);

    // transfer from camera to pixel coordinates
    x_projected = (K_2[0] * x_dist) + K_2[2];
    y_projected = (K_2[1] * y_dist) + K_2[3];

    // calculate residuals, note: ceres squares these automatically
    residuals[0] = T(weight_) * (residuals[2] = x_projected - T(p_1_[0]));
    residuals[1] = T(weight_) * (residuals[3] = y_projected - T(p_1_[1]));

    return true;

}

// Explicit instantiation
template bool Projection_Error_Intrinsics_Brown::operator()(const double* const K, const double* const dist,
const double* const rot_P, const double* const t_P, double* residuals) const;
template bool Projection_Error_Intrinsics_Rational::operator()(const double* const K, const double* const dist,
const double* const rot_P, const double* const t_P, double* residuals) const;
template bool Projection_Error_Extrinsics_Brown::operator()(const double* const R_rod_cam,
const double* const t_cam, const double* const R_rod_P, const double* const t_P,double* residuals) const;
template bool Projection_Error_Extrinsics_Rational::operator()(const double* const R_rod_cam,
const double* const t_cam, const double* const R_rod_P, const double* const t_P,double* residuals) const;

template bool Projection_Error_Intrinsics_Brown::operator()(ceres::Jet<double, 15> const*,
ceres::Jet<double, 15> const*, ceres::Jet<double, 15> const*, ceres::Jet<double, 15> const*, ceres::Jet<double, 15>*) const;
template bool Projection_Error_Intrinsics_Rational::operator()(ceres::Jet<double, 18> const*,
ceres::Jet<double, 18> const*, ceres::Jet<double, 18> const*, ceres::Jet<double, 18> const*, ceres::Jet<double, 18>*) const;
template bool Projection_Error_Extrinsics_Brown::operator()(ceres::Jet<double, 12> const*,
ceres::Jet<double, 12> const*, ceres::Jet<double, 12> const*, ceres::Jet<double, 12> const*, ceres::Jet<double, 12>*) const;
template bool Projection_Error_Extrinsics_Rational::operator()(ceres::Jet<double, 12> const*,
ceres::Jet<double, 12> const*, ceres::Jet<double, 12> const*, ceres::Jet<double, 12> const*, ceres::Jet<double, 12>*) const;


///////////////////////////   Rational Cost    /////////////////////////////////

template <typename T>
bool Rational_Cost::operator()(const T* const dist, T* residuals) const{
    T d = T(1.0);
    T c = dist[3];
    T b = dist[4];
    T a = dist[5];

    // Find roots according to Cardano's formula for third order polynomials
    // for 1+k4*x+k5*x^2+k6*x^3
    T p = ( 9.0 * a *c - 3.0 * b * b ) / (9.0 * a * a);
    T q = (-9.0 * a * b * c + 27.0 * a * a * d + 2.0 * b * b * b)/(27.0 * a * a * a);

    T dis = - q * q / 4.0 - p * p * p / 27.0;
    T t_1 = - q/2.0 + ceres::sqrt(-dis);
    T t_2 = - q/2.0 - ceres::sqrt(-dis);

    T weight = T(weight_) * T(num_reprojection_residuals_);
    T r_max_h = T(min_r_ / 2.0);
    T cost;
    if(ceres::abs(dis) < 0.000001){
        if(ceres::abs(p) < 0.000001){ // one solution (3x)
            T x = - b / (3.0 * a);
            cost = ceres::exp(-ceres::pow((x - r_max_h)/r_max_h, 2.0));
        }
        else{ // two solutions (1x, one touches the axis -> 2x)
            T x_1 = - b / (3.0 * a) + 3.0 * q / p;
            T x_23 = - b / (3.0 * a) - 3.0 * q / (2.0 * p);

            cost = std::max(ceres::exp(-ceres::pow((x_1 - r_max_h)/r_max_h, 2.0)),
                            ceres::exp(-ceres::pow((x_23 - r_max_h)/r_max_h, 2.0)));
        }
    }
    else if(dis < 0.0){ // exact one real solution (1x) + 2 complex (1x)
        T x = - b / (3.0 * a) + ceres::cbrt(t_1) + ceres::cbrt(t_2);
        cost = ceres::exp(-ceres::pow((x - r_max_h)/r_max_h, 2.0));
    }
    else{ // three separate real solutions (all 1x)
        T acos_term = ceres::acos(-q/2.0 * ceres::sqrt(-27.0/(p*p*p)))/3.0;
        T fac = ceres::sqrt(-4.0 * p / 3.0);
        T x_1 = -b / (3.0 * a);
        T x_2 = x_1 - fac * ceres::cos(acos_term + T(M_PI)/3.0);
        T x_3 = x_1 - fac * ceres::cos(acos_term - T(M_PI)/3.0);
        x_1 = x_1 - fac * ceres::cos(acos_term);

        cost = std::max(ceres::exp(-ceres::pow((x_1 - r_max_h)/r_max_h, 2.0)),
                        ceres::exp(-ceres::pow((x_2 - r_max_h)/r_max_h, 2.0)));
        cost = std::max(ceres::exp(-ceres::pow((x_3 - r_max_h)/r_max_h, 2.0)),
                        cost);
    }

    residuals[0] = weight * cost;

    return true;
}
