#include "pose_cost.h"

Pose_Cost_Data::Pose_Cost_Data(ceres::CRSMatrix& J,
                               const std::vector<Eigen::Vector3d>& pattern_points,
                               const std::vector<Eigen::Vector3d>& outer_pattern_points,
                               const std::vector<Cam_Parameters>& cam_params,
                               const int& distortion_model,
                               const int& mode,
                               const double &min_pattern_dist,
                               const double &max_pattern_dist){
    /// Eigen wrapper for the ceres::CRSMatrix which results from the intrinsics
    /// optimization
    Eigen::MappedSparseMatrix<double, 1 , int> J_sparse
            = Eigen::MappedSparseMatrix<double,1,int>(J.num_rows,
                                                      J.num_cols,
                                                      J.values.size(),
                                                      J.rows.data(),
                                                      J.cols.data(),
                                                      J.values.data());
    Eigen::MatrixXd J_(J_sparse);
    Eigen::MatrixXd JtJ = J_.transpose() * J_;

    distortion_model_ = distortion_model;
    mode_ = mode;
    int num_distortion_params = (distortion_model == CALIB_BROWN) ? 3 : 6;
    int num_cam_params = (mode_ == OPT_MODE_TRAFO) ? 6 : 4 + 2 + num_distortion_params;
    int num_ext_pattern_params = 6;
    int num_blocks = (JtJ.cols() - num_cam_params) / num_ext_pattern_params;

    U_ = JtJ.block(0,0,num_cam_params,num_cam_params);
    V_inv_ = Eigen::MatrixXd::Zero((num_blocks+1) * num_ext_pattern_params,
                                   (num_blocks+1) * num_ext_pattern_params);
    for(int block = 0; block < num_blocks; block++){
        V_inv_.block(block * num_ext_pattern_params, block * num_ext_pattern_params, num_ext_pattern_params,
                     num_ext_pattern_params) = JtJ.block(num_cam_params + block * num_ext_pattern_params,
                                                         num_cam_params + block * num_ext_pattern_params,
                                                         num_ext_pattern_params, num_ext_pattern_params).inverse();
    }
    W_ = Eigen::MatrixXd::Zero(num_cam_params, (num_blocks+1) * num_ext_pattern_params);
    W_.block(0, 0, num_cam_params, num_blocks*num_ext_pattern_params)
            = JtJ.block(0, num_cam_params, num_cam_params, num_blocks*num_ext_pattern_params);

    pattern_points_ = pattern_points;
    outer_pattern_points_ = outer_pattern_points;
    cam_params_ = cam_params;
    min_pattern_dist_ = min_pattern_dist;
    max_pattern_dist_ = max_pattern_dist;
}

double Pose_Cost_Data::getTrace(){
    Eigen::MatrixXd S = (U_ - W_ *V_inv_ * W_.transpose()).inverse();
    return S.trace();
}

double Pose_Cost::evaluate(const std::vector<double>& pose,
                           std::vector<double>& grad, void* data){

    // Get the relevant data
    Pose_Cost_Data* data_ = static_cast<Pose_Cost_Data*>(data);
    std::vector<double> R_rod = {pose[0], pose[1], pose[2]};
    // Mind the view pyramid -> Rescale x and y with respect to z distance to cam
    std::vector<double> t = {pose[3] * pose[5] / data_->max_pattern_dist_,
                             pose[4] * pose[5] / data_->max_pattern_dist_,
                             pose[5]};

    // Create additional parts A_m+1 and B_m+1 for Jacobian extension
    int num_distortion_params = (data_->distortion_model_ == CALIB_BROWN) ? 3 : 6;
    // Calculate number of parameters used
    int num_cam_params = (data_->mode_ == OPT_MODE_TRAFO) ? 6 : 4 + 2 + num_distortion_params;
    int num_ext_params = 6;
    int num_res_per_eval = (data_->mode_ == OPT_MODE_RGB || data_->mode_ == OPT_MODE_IR) ? 2 : 4;

    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(num_res_per_eval * data_->pattern_points_.size(), num_cam_params);
    Eigen::MatrixXd B = Eigen::MatrixXd::Zero(num_res_per_eval * data_->pattern_points_.size(), num_ext_params);

    for(int row = 0; row < data_->pattern_points_.size(); row++){
        // Project pattern point with given pose to image coordinates to create
        // the 3D/2D correspondences
        const Eigen::Vector3d& pattern_point = data_->pattern_points_[row];

        // Rotate point
        double P_pattern[3] = {double(pattern_point[0]), double(pattern_point[1]), double(pattern_point[2])};
        double P_world[3];
        ceres::AngleAxisRotatePoint(R_rod.data(), P_pattern, P_world);

        // Translate point
        P_world[0] += t[0];
        P_world[1] += t[1];
        P_world[2] += t[2];

        double P_cam_1[3] = {P_world[0], P_world[1], P_world[2]};
        Eigen::Vector2d pixel_1;
        if(data_->mode_ == OPT_MODE_RGB){
            // Project to image plane
            pixel_1 = data_->cam_params_[0].projectCamToPixel(
                        Eigen::Vector3d(P_cam_1[0],P_cam_1[1],P_cam_1[2]));
        }


        double P_cam_2[3];
        Eigen::Vector2d pixel_2;
        if(data_->mode_ == OPT_MODE_IR || data_->mode_ == OPT_MODE_TRAFO){
            // Translate point
            Eigen::Vector3d& t_cam = data_->cam_params_[1].t_;
            P_world[0] -= t_cam[0];
            P_world[1] -= t_cam[1];
            P_world[2] -= t_cam[2];

            ceres::AngleAxisRotatePoint(data_->cam_params_[1].R_rod_.data(),
                    P_world, P_cam_2);

            // Project to image plane
            pixel_2 = data_->cam_params_[1].projectCamToPixel(
                        Eigen::Vector3d(P_cam_2[0],P_cam_2[1],P_cam_2[2]));
        }

        // Calculate derivatives, i.e. the Jacobi matrix extension using
        // ceres::AutoDiffCostFunctions
        if(data_->mode_ == OPT_MODE_RGB || data_->mode_ == OPT_MODE_IR){
            // Calculate derivatives based on the intrinsics cost function
            size_t cam_idx = (data_->mode_ == OPT_MODE_IR) ? 1 : 0;
            Eigen::Vector2d pixel = (data_->mode_ == OPT_MODE_IR) ? pixel_2 : pixel_1;

            ceres::CostFunction* cost_function = nullptr;
            if(data_->cam_params_[cam_idx].distortion_model_ == CALIB_BROWN){
                cost_function = new ceres::AutoDiffCostFunction<Projection_Error_Intrinsics_Brown, 2, 4, 5, 3, 3>(
                            new Projection_Error_Intrinsics_Brown(pixel, pattern_point,
                                                                  data_->cam_params_[cam_idx].R_rod_,
                                                                  data_->cam_params_[cam_idx].t_));
            }
            else{
                cost_function = new ceres::AutoDiffCostFunction<Projection_Error_Intrinsics_Rational, 2, 4, 8, 3, 3>(
                            new Projection_Error_Intrinsics_Rational(pixel, pattern_point,
                                                                     data_->cam_params_[cam_idx].R_rod_,
                                                                     data_->cam_params_[cam_idx].t_));
            }

            const double* params[4] = {data_->cam_params_[cam_idx].opt_K_.data(),
                                       data_->cam_params_[cam_idx].opt_dist_.data(),
                                       R_rod.data(), t.data()};
            double deriv_K[2*4] = {0};
            double deriv_dist[2*(2 + num_distortion_params)] = {0};
            double deriv_R[2*3] = {0};
            double deriv_t[2*3] = {0};
            double* deriv[4] = {deriv_K, deriv_dist, deriv_R, deriv_t};
            double residuals[cost_function->num_residuals()] = {0};
            cost_function->Evaluate(params, residuals, deriv);

            // Save intrinsic derivatives
            for(int param_idx = 0; param_idx < 4; param_idx++){
                A(2*row, param_idx) = deriv[0][param_idx];
                A(2*row+1, param_idx) = deriv[0][4 + param_idx];
            }
            for(int param_idx = 0; param_idx < (2 + num_distortion_params); param_idx++){
                A(2*row, 4 + param_idx) = deriv[1][param_idx];
                A(2*row+1, 4 + param_idx) = deriv[1][(2 + num_distortion_params) + param_idx];
            }

            // Save extrinsic derivatives
            for(int param_idx = 0; param_idx < 3; param_idx++){
                B(2*row, param_idx) = deriv[2][param_idx];
                B(2*row + 1, param_idx) = deriv[2][3 + param_idx];
            }
            for(int param_idx = 0; param_idx < 3; param_idx++){
                B(2*row, 3 + param_idx) = deriv[3][param_idx];
                B(2*row + 1, 3 + param_idx) = deriv[3][3 + param_idx];
            }
        }
        else{
            // calculate derivatives based on the extrinsics cost function
            size_t cam_idx = (data_->mode_ == OPT_MODE_IR) ? 1 : 0;
            ceres::CostFunction* cost_function = nullptr;
            if(data_->cam_params_[cam_idx].distortion_model_ == CALIB_BROWN){
                cost_function = new ceres::AutoDiffCostFunction<Projection_Error_Extrinsics_Brown, 4, 3, 3, 3, 3>(
                            new Projection_Error_Extrinsics_Brown(pixel_1, pixel_2, pattern_point,
                                                                  data_->cam_params_));
            }
            else{
                cost_function = new ceres::AutoDiffCostFunction<Projection_Error_Extrinsics_Rational, 4, 3, 3, 3, 3>(
                            new Projection_Error_Extrinsics_Rational(pixel_1, pixel_2, pattern_point,
                                                                     data_->cam_params_));
            }

            const double* params[4] = {data_->cam_params_[1].R_rod_.data(),
                                       data_->cam_params_[1].t_.data(),
                                       R_rod.data(), t.data()};
            double deriv_R_cam[4*3] = {0};
            double deriv_t_cam[4*3] = {0};
            double deriv_R[4*3] = {0};
            double deriv_t[4*3] = {0};
            double* deriv[4] = {deriv_R_cam, deriv_t_cam, deriv_R, deriv_t};
            double residuals[cost_function->num_residuals()] = {0};
            cost_function->Evaluate(params, residuals, deriv);

            // save cam parameter derivatives
            for(int param_idx = 0; param_idx < 3; param_idx++){
                A(4*row,param_idx) = deriv[0][param_idx];
                A(4*row+1,param_idx) = deriv[0][3+param_idx];
                A(4*row+2,param_idx) = deriv[0][6+param_idx];
                A(4*row+3,param_idx) = deriv[0][9+param_idx];
            }
            for(int param_idx = 0; param_idx < 3; param_idx++){
                A(4*row, 3 + param_idx) = deriv[1][param_idx];
                A(4*row+1, 3 + param_idx) = deriv[1][3+param_idx];
                A(4*row+2, 3 + param_idx) = deriv[1][6+param_idx];
                A(4*row+3, 3 + param_idx) = deriv[1][9+param_idx];
            }

            // save extrinsic derivatives
            for(int param_idx = 0; param_idx < 3; param_idx++){
                B(4*row, param_idx) = deriv[2][param_idx];
                B(4*row+1, param_idx) = deriv[2][3+param_idx];
                B(4*row+2, param_idx) = deriv[2][6+param_idx];
                B(4*row+3, param_idx) = deriv[2][9+param_idx];
            }
            for(int param_idx = 0; param_idx < 3; param_idx++){
                B(4*row, 3 + param_idx) = deriv[3][param_idx];
                B(4*row+1, 3 + param_idx) = deriv[3][3+param_idx];
                B(4*row+2, 3 + param_idx) = deriv[3][6+param_idx];
                B(4*row+3, 3 + param_idx) = deriv[3][9+param_idx];
            }

        }


    }

    // calculate the inverted sub-matrix by extending U, V and W
    Eigen::MatrixXd U = data_->U_;
    U += A.transpose()*A;
    Eigen::MatrixXd W = data_->W_;
    W.block(0, W.cols() - num_ext_params, num_cam_params, num_ext_params)
            = A.transpose() * B;
    Eigen::MatrixXd V_inv = data_->V_inv_;
    V_inv.block(V_inv.rows() - num_ext_params, V_inv.cols() - num_ext_params,
                num_ext_params, num_ext_params) = (B.transpose() * B).inverse();

    Eigen::MatrixXd S = (U - W *V_inv * W.transpose()).inverse();

    // calculate the trace of this matrix
    double trace = S.trace();
    return S.trace();
}

double Pose_Cost::constraints(const std::vector<double>& pose, std::vector<double>& grad, void* data){

    // Get the relevant data
    Pose_Cost_Data* data_ = static_cast<Pose_Cost_Data*>(data);
    Eigen::Vector3d R_rod(pose[0], pose[1], pose[2]);
    std::vector<double> t = {pose[3] * pose[5] / data_->max_pattern_dist_,
                             pose[4] * pose[5] / data_->max_pattern_dist_,
                             pose[5]};

    std::vector<Eigen::Vector2d> pixels;

    // Project outer pattern points to image coordinates
    for(const Eigen::Vector3d& pattern_point : data_->outer_pattern_points_){
        // Rotate point
        double P_pattern[3] = {double(pattern_point[0]), double(pattern_point[1]), double(pattern_point[2])};
        double P_cam[3];
        ceres::AngleAxisRotatePoint(R_rod.data(), P_pattern, P_cam);

        // Translate point
        P_cam[0] += t[0];
        P_cam[1] += t[1];
        P_cam[2] += t[2];

        // Project to image plane
        Eigen::Vector4d p_cam(P_cam[0], P_cam[1], P_cam[2], 1.0);
        pixels.push_back(data_->cam_params_[0].projectCamToPixel(
                    Eigen::Vector3d(p_cam[0],p_cam[1],p_cam[2])));
    }

    // Check if within FoV
    const int offset = 0;
    data_->num_opt_steps++;
    for(const Eigen::Vector2d& pixel : pixels){
        if(pixel[0] < offset || pixel[1] < offset
                || pixel[0] > data_->cam_params_[0].resolution_x - offset - 1
                || pixel[1] > data_->cam_params_[0].resolution_y - offset - 1){
            data_->num_failed_opt_steps++;
            return 1.0;
        }
    }

    return -1.0;
}


