#pragma once

#include "Optimization/projection_errors.h"

///
/// \brief The Pose_Cost_Data class contains the parts of the pose estimation
///                                 which can be precalculated based on the
///                                 Jacobi matrix of the intrinsics optimization
/// Source: https://openaccess.thecvf.com/content_ICCV_2019/papers/
///         Peng_Calibration_Wizard_A_Guidance_System_for_Camera_Calibration
///         _Based_on_ICCV_2019_paper.pdf
///
class Pose_Cost_Data{

public:

    ///
    /// \brief Pose_Cost_Data
    ///
    Pose_Cost_Data(){}

    ///
    /// \brief Pose_Cost_Data
    /// \param J
    /// \param pattern_points
    /// \param outer_pattern_points
    /// \param cam_params
    /// \param distortion_model
    /// \param mode
    /// \param min_pattern_dist
    /// \param max_pattern_dist
    ///
    Pose_Cost_Data(ceres::CRSMatrix& J,
                   const std::vector<Eigen::Vector3d>& pattern_points,
                   const std::vector<Eigen::Vector3d>& outer_pattern_points,
                   const std::vector<Cam_Parameters>& cam_params,
                   const int &distortion_model,
                   const int &mode,
                   const double& min_pattern_dist,
                   const double& max_pattern_dist);

    ///
    /// \brief getTrace
    /// \return
    ///
    double getTrace();

    /// Using the decomposition J^tJ = (U W; W^t V) with
    /// U = \sum_i A_i^t*A_i
    Eigen::MatrixXd U_;
    /// W = (A_1^t*B_1  ...  A_m^t*B_m  A_m+1^t*B_m+1) with B_m+1= empty new
    /// block
    Eigen::MatrixXd W_;
    /// and V = \diag (B_1^t*B_1,...,B_m^t*B_m, B_m+1^t*B_m+1) with B_m+1= empty
    /// new block

    /// Then the upper left sub-matrix of (J^tJ)^-1 is given by
    /// S = (U-WV^-1W^t)^-1 for which V^-1 is precalculated
    Eigen::MatrixXd V_inv_;

    /// Pattern points in pattern coordinate system
    std::vector<Eigen::Vector3d> pattern_points_;
    /// Outermost pattern points used to determine whether the pattern with a
    /// proposed pose is completely visible for the camera
    std::vector<Eigen::Vector3d> outer_pattern_points_;

    /// Camera parameters
    std::vector<Cam_Parameters> cam_params_;

    /// The distortion model defines the number of intrinsic camera parameters
    int distortion_model_;

    /// Optimization mode
    int mode_;

    ///
    double max_pattern_dist_;
    double min_pattern_dist_;

    ///
    int num_opt_steps = 0;
    int num_failed_opt_steps = 0;

private:

    ///
    /// \brief calculateDecomposition
    ///
    void calculateDecomposition();

};

///
///
///
namespace Pose_Cost{

///
/// \brief evaluate
/// \param pose
/// \param grad
/// \param data
/// \return
///
double evaluate(const std::vector<double>& pose, std::vector<double>& grad, void* data);

///
/// \brief constraints
/// \param pose
/// \param grad
/// \param data
/// \return
///
double constraints(const std::vector<double>& pose, std::vector<double>& grad, void* data);

};
