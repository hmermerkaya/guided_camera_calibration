////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///         Pattern implementation for classical checkerboards               ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Patterns/pattern.h"
#include <QObject>

class Checkerboard : public Pattern{
    Q_OBJECT

public:

    ///
    /// \brief Checkerboard is the default constructor
    ///
    Checkerboard();

    ///
    /// \brief patternProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> patternProperties();

    ///
    /// \brief detectorProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> detectorProperties();

    ///
    /// \brief getProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getProperties();

    ///
    /// \brief getPattern
    /// \return
    ///
    QImage getPattern();

public slots:
    ///
    /// \brief createPattern
    /// \return
    ///
    bool createPattern();

    ///
    /// \brief detectPattern
    /// \param frame
    /// \return
    ///
    bool detectPattern(std::shared_ptr<const Frame>& frame);

    ///
    /// \brief detectPattern
    /// \param frame
    /// \param detection
    /// \return
    ///
    bool detectPattern(const QImage& frame, Detection detection);

    ///
    /// \brief setOptimizationMode is the setter for the optimization mode which
    ///                            determines the images (rgb and/or ir) used
    ///                            for detection
    /// \param mode
    ///
    void setOptimizationMode(const int& mode){
        mode_ = mode;
    }

    ///
    /// \brief setProposedPose is used to receive the next pose proposal from
    ///                        the optimization, which can e.g. be used to
    ///                        properly scale the IR image brightness
    /// \param pose
    ///
    void setProposedPose(const Pose& pose);

    ///
    /// \brief detectorModeChanged
    /// \param mode
    ///
    void detectorModeChanged(const int& mode);

    ///
    /// \brief refinementModeChanged
    /// \param enabled
    ///
    void refinementModeChanged(const bool& enabled);

private:

    std::shared_ptr<Property_Spin> width_;
    std::shared_ptr<Property_Spin> height_;
    std::shared_ptr<Property_Slider> board_border_;
    std::shared_ptr<Property_Spin_F> square_size_in_mm_;
    std::shared_ptr<Property_Selector> detector_flags_;
    std::shared_ptr<Property_Check> refinement_;

    std::vector<std::shared_ptr<Property>> pattern_properties_;
    std::vector<std::shared_ptr<Property>> detector_properties_;

    std::vector<std::shared_ptr<Property>> all_properties_;

    cv::Mat min_pattern_img_;
    QImage min_pattern_qimg_;

    Partition partition_;

    long last_timestamp_ = 0;

    int mode_ = -1;

    float ir_scale_ = 1.0f;

    bool checkerboard_created_ = false;

    int current_flags_ = cv::CALIB_CB_ADAPTIVE_THRESH
            + cv::CALIB_CB_NORMALIZE_IMAGE
            + cv::CALIB_CB_FAST_CHECK;
    bool refinement_enabled_ = true;
};
