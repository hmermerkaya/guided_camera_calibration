////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///     Pattern implementation for ChArUco boards, a combination of          ///
///     classical checkerboards and ArUco markers for unique corner          ///
///                             identification                               ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Patterns/pattern.h"

#include <opencv4/opencv2/aruco/charuco.hpp>

//aruco codes
const std::vector<int> ARUCO_CODES = {
    cv::aruco::DICT_4X4_50,
    cv::aruco::DICT_4X4_100,
    cv::aruco::DICT_4X4_250,
    cv::aruco::DICT_4X4_1000,
    cv::aruco::DICT_5X5_50,
    cv::aruco::DICT_5X5_100,
    cv::aruco::DICT_5X5_250,
    cv::aruco::DICT_5X5_1000,
    cv::aruco::DICT_6X6_50,
    cv::aruco::DICT_6X6_100,
    cv::aruco::DICT_6X6_250,
    cv::aruco::DICT_6X6_1000,
    cv::aruco::DICT_7X7_50,
    cv::aruco::DICT_7X7_100,
    cv::aruco::DICT_7X7_250,
    cv::aruco::DICT_7X7_1000
};

class ChArUco : public Pattern{
    Q_OBJECT

public:

    ///
    /// \brief ChArUco is the default constructor
    ///
    ChArUco();

    ///
    /// \brief patternProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> patternProperties();

    ///
    /// \brief detectorProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> detectorProperties();

    ///
    /// \brief getProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getProperties();

    ///
    /// \brief getPattern
    /// \return
    ///
    QImage getPattern();

public slots:
    ///
    /// \brief createPattern
    /// \return
    ///
    bool createPattern();

    ///
    /// \brief detectPattern
    /// \param frame
    /// \return
    ///
    bool detectPattern(std::shared_ptr<const Frame>& frame);

    ///
    /// \brief detectPattern
    /// \param frame
    /// \param detection
    /// \return
    ///
    bool detectPattern(const QImage& frame, Detection detection);

    ///
    /// \brief setOptimizationMode is the setter for the optimization mode which
    ///                            determines the images (rgb and/or ir) used
    ///                            for detection
    /// \param mode
    ///
    void setOptimizationMode(const int& mode){
        mode_ = mode;
    }

    ///
    /// \brief setProposedPose is used to receive the next pose proposal from
    ///                        the optimization, which can e.g. be used to
    ///                        properly scale the IR image brightness
    /// \param pose
    ///
    void setProposedPose(const Pose& pose);

    ///
    /// \brief refinementMethodChanged
    /// \param method
    ///
    void refinementMethodChanged(const int& method);

private:

    std::shared_ptr<Property_Spin> width_;
    std::shared_ptr<Property_Spin> height_;
    std::shared_ptr<Property_Slider> board_border_;
    std::shared_ptr<Property_Selector> marker_size_;
    std::shared_ptr<Property_Spin> marker_border_size_;
    std::shared_ptr<Property_Spin_F> marker_square_ratio_;
    std::shared_ptr<Property_Spin_F> square_size_in_mm_;
    std::shared_ptr<Property_Selector> refinement_method_;

    std::vector<std::shared_ptr<Property>> pattern_properties_;
    std::vector<std::shared_ptr<Property>> detector_properties_;

    std::vector<std::shared_ptr<Property>> all_properties_;

    QStringList marker_size_list_ = {"4x4", "5x5", "6x6", "7x7"};
    std::vector<int> dict_size_list_ = {50, 100, 250, 1000};

    cv::Ptr<cv::aruco::Dictionary> dict_;
    cv::Ptr<cv::aruco::CharucoBoard> board_;
    cv::Ptr<cv::aruco::DetectorParameters> detector_params_;

    cv::Mat min_pattern_img_;
    QImage min_pattern_qimg_;

    Partition partition_;

    long last_timestamp_ = 0;

    int mode_ = -1;

    float ir_scale_ = 1.0f;

    bool arucoboard_created_ = false;
};

