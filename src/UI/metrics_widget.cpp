#include "metrics_widget.h"
#include "ui_metrics_widget.h"

Metrics_Widget::Metrics_Widget(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Metrics_Widget)
{
    ui_->setupUi(this);
    param_types_ = {PARAM_RMSE, PARAM_TRACE, PARAM_FX, PARAM_FY};

    QGridLayout* layout = static_cast<QGridLayout*>(ui_->box_graphs->layout());
    for(int row = 0; row < 2; row++){
        for(int col = 0; col < 2; col++){
            graphs_.push_back(std::shared_ptr<Parameter_Graph>(new Parameter_Graph()));
            layout->addWidget(graphs_.back().get(), row, col);
        }
    }

    for(size_t i = 0; i < 4; i++){
        const std::shared_ptr<Parameter_Graph>& graph = graphs_[i];
        graph->setParameterType(param_types_[i]);
        connect(graph.get(), &Parameter_Graph::newHistoryRequested, this, &Metrics_Widget::sendHistory);
    }

    connect(ui_->button_save_graphs, &QPushButton::clicked, this, &Metrics_Widget::saveGraphs);
}

Metrics_Widget::~Metrics_Widget(){
    delete ui_;
}

void Metrics_Widget::updateParameters(const std::vector<Cam_Parameters>& params){

    if(history_.empty()){
        for(const Cam_Parameters& p : params){
            history_.push_back({});
        }
    }

    for(size_t i = 0; i < params.size(); i++){
        const Cam_Parameters& p = params[i];
        history_[i].push_back({p.rmse_, p.trace_, p.fx_, p.fy_, p.cx_, p.cy_,
                               p.k_[0], p.k_[1], p.k_[2], p.k_[3], p.k_[4],
                               p.k_[5], p.p1_, p.p2_});
    }

    for(size_t i = 0; i < 4; i++){
        const std::shared_ptr<Parameter_Graph>& graph = graphs_[i];
        graph->extendParameterSeries(history_[0].back()[param_types_[i]]);
    }
}

void Metrics_Widget::sendHistory(const int& param_type){

    if(history_.size() == 0){
        return;
    }

    std::vector<double> param_history;
    for(const std::vector<double>& params : history_[0]){
        param_history.push_back(params[param_type]);
    }

    Parameter_Graph* sender = static_cast<Parameter_Graph*>(this->sender());
    sender->setParameterSeries(param_history);
}

void Metrics_Widget::saveGraphs(){
    // Ask the user to choose a file location to save the screenshot
    QString filename = QFileDialog::getSaveFileName(nullptr, "Save Screenshot", "", "Images (*.png, *.jpg)");

    if (!filename.isEmpty()){

        if(!(filename.endsWith(".png") || filename.endsWith(".jpg"))){
            filename += ".png";
        }

        // Capture the widget screenshot
        QPixmap pixmap = ui_->box_graphs->grab();

        // Save the screenshot to the selected file location
        pixmap.save(filename);
    }
    else{
        ERROR("No file chosen.");
    }
}
