#include "camera_parameter_widget.h"
#include "ui_camera_parameter_widget.h"

Camera_Parameter_Widget::Camera_Parameter_Widget(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Camera_Parameter_Widget)
{
    ui_->setupUi(this);
    ui_->button_accept_modified->setVisible(false);

    connect(ui_->button_save, &QPushButton::clicked,
            this, &Camera_Parameter_Widget::saveCameraParameters);
    connect(ui_->button_load, &QPushButton::clicked,
            this, &Camera_Parameter_Widget::loadCameraParameters);
    connect(ui_->button_visualize, &QPushButton::clicked,
            this, &Camera_Parameter_Widget::requestVisualization);
    connect(ui_->button_accept_modified, &QPushButton::clicked, [this](){
        updateFromGUI();
        parametersUpdated(this->parameters_);
        ui_->button_accept_modified->setVisible(false);
    });


    // Every manual parameter change leads to the accept button being visible
    connect(ui_->spinbox_cx, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_cy, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_fx, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_fy, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_k1, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_k2, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_k3, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_k4, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_k5, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_k6, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_p1, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
    connect(ui_->spinbox_p2, &QDoubleSpinBox::editingFinished, this, &Camera_Parameter_Widget::setAcceptButtonVisible);
}

Camera_Parameter_Widget::~Camera_Parameter_Widget(){
    delete ui_;
}

void Camera_Parameter_Widget::setAcceptButtonVisible(){
    ui_->button_accept_modified->setVisible(true);
}

void Camera_Parameter_Widget::setCameraParameters(
        const std::vector<Cam_Parameters>& parameters){

    parameters_ = parameters;

    // set first cam to be default
    if(currently_selected_cam == -1){
        currently_selected_cam = 0;
        for(const Cam_Parameters& params : parameters){
            ui_->selector_camera->addItem(
                        QString::fromStdString(params.camera_name_));
        }
        ui_->selector_camera->setCurrentIndex(0);

        connect(ui_->selector_camera, &QComboBox::currentIndexChanged,
                this, &Camera_Parameter_Widget::updateGUI);
    }

    updateGUI(currently_selected_cam);
}

void Camera_Parameter_Widget::setVisualizationAvailable(const bool& available){
    ui_->button_visualize->setHidden(!available);
}

void Camera_Parameter_Widget::updateGUI(const int& index){
    currently_selected_cam = index;
    Cam_Parameters& params = parameters_[currently_selected_cam];

    // set data to GUI
    ui_->spinbox_fx->setValue(params.fx_);
    ui_->spinbox_fy->setValue(params.fy_);
    ui_->spinbox_cx->setValue(params.cx_);
    ui_->spinbox_cy->setValue(params.cy_);

    ui_->spinbox_k1->setValue(params.k_[0]);
    ui_->spinbox_k2->setValue(params.k_[1]);
    ui_->spinbox_k3->setValue(params.k_[2]);
    ui_->spinbox_k4->setValue(params.k_[3]);
    ui_->spinbox_k5->setValue(params.k_[4]);
    ui_->spinbox_k6->setValue(params.k_[5]);
    ui_->spinbox_p1->setValue(params.p1_);
    ui_->spinbox_p2->setValue(params.p2_);

}

void Camera_Parameter_Widget::updateFromGUI(){
    Cam_Parameters& params = parameters_[currently_selected_cam];

    // set data to GUI
    params.fx_ = ui_->spinbox_fx->value();
    params.fy_ = ui_->spinbox_fy->value();
    params.cx_ = ui_->spinbox_cx->value();
    params.cy_ = ui_->spinbox_cy->value();
    params.p1_ = ui_->spinbox_p1->value();
    params.p2_ = ui_->spinbox_p2->value();
    params.k_[0] = ui_->spinbox_k1->value();
    params.k_[1] = ui_->spinbox_k2->value();
    params.k_[2] = ui_->spinbox_k3->value();
    params.k_[3] = ui_->spinbox_k4->value();
    params.k_[4] = ui_->spinbox_k5->value();
    params.k_[5] = ui_->spinbox_k6->value();
}

void Camera_Parameter_Widget::loadCameraParameters(){
    std::string error;
    std::vector<Cam_Parameters> parameters;
    if(!IO::loadCameraParameters(&parameters, &error)){
        ERROR(QString::fromStdString(error));
    }
    else{
        for(size_t i = 0; i < this->parameters_.size(); i++){
            for(size_t j = 0; j < parameters.size(); j++){
                if(this->parameters_[i].camera_name_.compare(parameters[j].camera_name_) == 0){
                    this->parameters_[i] = parameters[j];
                    updateGUI(i);
                }
            }
        }


        STATUS("Parameters loaded.");
    }
}

void Camera_Parameter_Widget::saveCameraParameters(){
    std::string error;
    if(!IO::saveCameraParameters(this->parameters_, &error)){
        ERROR(QString::fromStdString(error));
    }
    else{
        emit parametersUpdated(this->parameters_);
        STATUS("Parameters successfully saved.");
    }
}

void Camera_Parameter_Widget::requestVisualization(){
    emit visualizationRequested(parameters_);
}
