#include "optimization_widget.h"
#include "ui_optimization_widget.h"

Optimization_Widget::Optimization_Widget(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Optimization_Widget)
{
    ui_->setupUi(this);
}

Optimization_Widget::~Optimization_Widget(){
    // unparent property widgets to avoid double deletion
    for(const std::shared_ptr<QWidget>& property : widget_list_intr_){
        property->setParent(nullptr);
    }
    for(const std::shared_ptr<QWidget>& property : widget_list_pose_){
        property->setParent(nullptr);
    }
    for(const std::shared_ptr<QWidget>& property : widget_list_general_){
        property->setParent(nullptr);
    }
    delete ui_;
}

void Optimization_Widget::setOptimizer(const std::shared_ptr<Optimizer>& optimizer){
    optimizer_ = optimizer;
    setProperties(optimizer_->propertyListIntr(), 0);
    setProperties(optimizer_->propertyListPose(), 1);
    setProperties(optimizer_->propertyListGeneral(), 2);
}

void Optimization_Widget::setProperties(const std::vector<std::shared_ptr<Property>>& list,
                                        const int& mode){
    if(mode == 0){
        widget_list_intr_ = list;
        QGridLayout* layout = static_cast<QGridLayout*>(ui_->frame_intrinsics->layout());
        for(const std::shared_ptr<Property>& property : list){
            layout->addWidget(property.get());
        }
    }
    else if(mode == 1){
        widget_list_pose_ = list;
        QGridLayout* layout = static_cast<QGridLayout*>(ui_->frame_pose->layout());
        for(const std::shared_ptr<Property>& property : list){
            layout->addWidget(property.get());
        }
    }
    else{
        widget_list_general_ = list;
        QGridLayout* layout = static_cast<QGridLayout*>(ui_->frame_general->layout());
        for(const std::shared_ptr<Property>& property : list){
            layout->addWidget(property.get());
        }
    }
}
