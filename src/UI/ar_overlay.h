#pragma once

#include "Common/data_types.h"

#include <chrono>
#include <QObject>

class AR_Overlay : public QObject{

    Q_OBJECT

public:
    ///
    /// \brief AR_Overlay
    ///
    AR_Overlay();

    ///
    ~AR_Overlay();

    ///
    /// \brief propertyList
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> propertyList(){return properties_;}

public slots:

    ///
    /// \brief updatePattern
    /// \param partition
    ///
    void updatePattern(const Partition& partition);

    ///
    /// \brief updateCameraParameters
    /// \param parameters
    ///
    void updateCameraParameters(const std::vector<Cam_Parameters>& parameters);

    ///
    /// \brief updatePose
    /// \param pose
    ///
    void updatePose(const Pose& pose);

    ///
    /// \brief checkNewDetection
    /// \param detection
    ///
    void checkNewDetection(const Detection_Frame& detection);

    ///
    /// \brief setOptimizationMode is the setter for the optimization mode which
    ///                            determines the images (rgb or ir) used
    ///                            for showing the AR overlay
    /// \param mode
    ///
    void setOptimizationMode(const int& mode, const int& submode){
        mode_ = mode;
        submode_ = submode;
        emit overlayChanged(partition_, true);
    }

private slots:

    ///
    /// \brief recalculateHomographies
    /// \return
    ///
    bool recalculateHomographies();

private:

    ///
    Partition partition_;

    ///
    std::vector<Cam_Parameters> parameters_;

    /// suggestion for the next calibration pattern pose
    Pose pose_;

    ///
    long overlay_match_time_in_ms_;

    ///
    std::vector<Detection> initial_matched_detection_;
    std::vector<Feature_Point> initial_outer_points_;

    ///
    float match_threshold_ = 25.0f;

    ///
    bool waiting_for_new_pose_ = false;

    /// optimization mode, which determines the image feed to show the AR
    /// overlay
    int mode_ = -1;
    int submode_ = -1;

    /// Counter for storing debug images
    int num_debug_imgs_ = 0;

    /// Properties that can be modified by the user via the GUI
    std::shared_ptr<Property_Check> store_images_;

    std::vector<std::shared_ptr<Property>> properties_;

signals:

    ///
    /// \brief overlayChanged
    /// \param partition
    /// \param tiles_changed
    ///
    void overlayChanged(const Partition& partition, const bool& tiles_changed);

    ///
    /// \brief detectionAxesCalculated
    /// \param axes
    ///
    void detectionAxesCalculated(const AxisPixels& axes);

    ///
    /// \brief matchedPartition
    /// \param partition
    ///
    void matchedPartition(const Partition& partition);

    ///
    /// \brief matchedDetection
    /// \param detection
    ///
    void matchedDetection(const std::vector<Detection>& detection);

    ///
    /// \brief requestNewPose
    ///
    void requestNewPose();

};
