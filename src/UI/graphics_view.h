#pragma once

#include <opencv4/opencv2/opencv.hpp>

#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QScrollBar>
#include <QTimer>

class Graphics_View : public QGraphicsView
{
    Q_OBJECT

public:
    ///
    /// \brief Graphics_View constructor
    ///
    Graphics_View(QWidget* parent = nullptr);

    ///
    /// \brief getScene
    /// \return
    ///
    std::shared_ptr<QGraphicsScene> getScene(){return scene_;}

public slots:

    ///
    /// \brief resetImage
    ///
    void resetImage();

    ///
    /// \brief setImage sets the image to display
    /// \param image    Image to display
    ///
    void setImage(const cv::Mat& image);

    ///
    /// \brief setImage sets the image to display
    /// \param image    Image to display
    ///
    void setImage(const QPixmap& image);

    ///
    /// \brief setImage sets the image to display
    /// \param image    Image to display
    ///
    void setImage(const QImage& image);

    ///
    /// \brief setZoom
    /// \param zoom
    ///
    void resetZoom(){
        float mirrored = (this->transform().m11() < 0.0) ? -1.0f : 1.0f;

        zoom_ = float(1.0/this->devicePixelRatioF());
        this->setTransform(QTransform(mirrored * zoom_,0,0,0,zoom_,0,0,0,1));
    }

    ///
    /// \brief fitViewToImage
    ///
    void fitViewToImage();

    ///
    /// \brief toggleMirrored
    ///
    void toggleMirrored(){
        this->setTransform(QTransform(-1.0f * this->transform().m11(),0,0,0,
                                      this->transform().m22(),0,0,0,1));
    }

private:
    // ui items to display
    std::shared_ptr<QGraphicsScene> scene_;
    std::shared_ptr<QGraphicsPixmapItem> pixmap_;

    // save last mouse position and scroll bar positions
    QPointF last_cursor_position_;
    float last_h_bar_pos_;
    float last_v_bar_pos_;
    // current image zoom level
    float zoom_ = 1.0;
    // saves if the mouse buttons are currently pressed
    bool left_mouse_pressed_ = false;
    bool right_mouse_pressed_ = false;
    // timer used in order to reduce the mouse move event calculation rate
    QTimer timer_;

    ///
    /// \brief mousePressEvent handles mouse clicks for starting a move in a zoomed in image
    /// \param event    Mouse event
    ///
    void mousePressEvent(QMouseEvent* event);

    ///
    /// \brief mouseReleaseEvent handles mouse releases for ending a move in a zoomed in image
    /// \param event    Mouse event
    ///
    void mouseReleaseEvent(QMouseEvent* event);

    ///
    /// \brief mouseMoveEvent handles mouse move events for moving in a zoomed in image
    /// \param event    Mouse event
    ///
    void mouseMoveEvent(QMouseEvent* event);

    ///
    /// \brief wheelEvent handles scrolling events for zooming into or out of an image
    /// \param event    Wheel event
    ///
    void wheelEvent(QWheelEvent* event);

signals:
    ///
    /// \brief imageSet
    ///
    void imageSet();
};


