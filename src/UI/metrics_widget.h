#pragma once

#include "Common/data_types.h"
#include "UI/parameter_graph.h"
#include "UI/logger.h"

#include <QWidget>

namespace Ui {
class Metrics_Widget;
}

class Metrics_Widget : public QWidget
{
    Q_OBJECT

public:

    ///
    /// \brief Metrics_Widget
    /// \param parent
    ///
    explicit Metrics_Widget(QWidget *parent = nullptr);

    ///
    ~Metrics_Widget();

public slots:

    ///
    /// \brief updateParameters
    /// \param params
    ///
    void updateParameters(const std::vector<Cam_Parameters> &params);

    ///
    /// \brief sendHistory
    /// \param param_type
    ///
    void sendHistory(const int& param_type);

private slots:

    ///
    /// \brief saveGraphs
    ///
    void saveGraphs();

private:
    ///
    Ui::Metrics_Widget* ui_;
    ///
    std::vector<int> param_types_;
    ///
    std::vector<std::shared_ptr<Parameter_Graph>> graphs_;
    /// History[cam_idx][set_idx][parameter_idx]
    std::vector<std::vector<std::vector<double>>> history_;

signals:
    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(const QString& msg, const int& type);

};
