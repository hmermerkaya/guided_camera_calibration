#include "graphics_view.h"

Graphics_View::Graphics_View(QWidget *parent){
    this->setParent(parent);
    viewport()->setMouseTracking(true); //activate mouse tracking
    timer_.setInterval(25); //40 hz update rate for mouse movements
    timer_.setSingleShot(true);

    this->setBackgroundBrush(QBrush(QColor(50,50,50)));

    resetZoom();

    scene_ = std::shared_ptr<QGraphicsScene>(new QGraphicsScene());
    pixmap_ = std::shared_ptr<QGraphicsPixmapItem>(new QGraphicsPixmapItem());
    scene_->addItem(pixmap_.get());
    setScene(scene_.get());
    show();
}

void Graphics_View::resetImage(){
    scene_->removeItem(pixmap_.get());
    pixmap_.reset();
    pixmap_ = std::shared_ptr<QGraphicsPixmapItem>(new QGraphicsPixmapItem());
    scene_->addItem(pixmap_.get());
}

void Graphics_View::setImage(const cv::Mat& image){
    QImage::Format format;
    if(image.type() == CV_8UC1){
        format = QImage::Format_Grayscale8;
    }
    else if(image.type() == CV_8UC3){
        format = QImage::Format_RGB888;
    }
    else if(image.type() == CV_8UC4){
        format = QImage::Format_RGBA8888;
    }
    else if(image.type() == CV_32F){//usually a depth image
        format = QImage::Format_RGB888;
        cv::Mat colored_image;

        image.convertTo(colored_image, CV_8UC1, 255.0/6000.0, 0);
        applyColorMap(colored_image,colored_image, cv::COLORMAP_JET);
        QImage qImage(colored_image.data, colored_image.cols, colored_image.rows, colored_image.step, format);
        setImage(QPixmap::fromImage(qImage));
        emit imageSet();
        return;
    }
    else{
        return;
    }

    QImage qImage(image.data, image.cols, image.rows, image.step, format);
    setImage(QPixmap::fromImage(qImage));
    emit imageSet();
}


void Graphics_View::setImage(const QPixmap& image){
    pixmap_->setPixmap(image);
    emit imageSet();
}

void Graphics_View::setImage(const QImage& image){
    pixmap_->setPixmap(QPixmap::fromImage(image));
    emit imageSet();
}

void Graphics_View::fitViewToImage(){
    resetZoom();
    int width = pixmap_->pixmap().width();
    int height = pixmap_->pixmap().height();
    int border = std::max(1, int(0.05 * float(std::max(width, height))));
    setSceneRect(0,0,pixmap_->pixmap().width(),pixmap_->pixmap().height());
    fitInView(-border, -border, width + 2*border, height + 2*border, Qt::KeepAspectRatio);
}

void Graphics_View::mousePressEvent(QMouseEvent* event){
    if(event->button() == Qt::LeftButton){
        //set initial positions of cursor and scrollbars
        last_cursor_position_ = event->globalPosition();
        last_h_bar_pos_ = float(this->horizontalScrollBar()->value());
        last_v_bar_pos_ = float(this->verticalScrollBar()->value());
        left_mouse_pressed_ = true;
    }
}

void Graphics_View::mouseReleaseEvent(QMouseEvent* event){
    if(event->button() == Qt::LeftButton){
        left_mouse_pressed_ = false;
    }
}

void Graphics_View::mouseMoveEvent(QMouseEvent* event){
    if(timer_.isActive()){//return in order to prevent signal flooding
        return;
    }

    if(left_mouse_pressed_){
        //translate scene by modifying scrollbars
        this->horizontalScrollBar()->setValue(last_h_bar_pos_ - (event->globalPosition().x()-last_cursor_position_.x()));
        this->verticalScrollBar()->setValue(last_v_bar_pos_ - (event->globalPosition().y()-last_cursor_position_.y()));

        //update current positions - needed in order to prevent the mouse from entering a dead (no translation) area
        last_cursor_position_ = event->globalPosition();
        last_h_bar_pos_ = float(this->horizontalScrollBar()->value());
        last_v_bar_pos_ = float(this->verticalScrollBar()->value());
    }
    timer_.start();
}

void Graphics_View::wheelEvent(QWheelEvent* event){
    //rescale the scene
    float old_zoom = zoom_;
    zoom_ += 0.001f*(float)event->angleDelta().y();
    this->scale(zoom_/old_zoom,zoom_/old_zoom);
    zoom_ = old_zoom;
}
