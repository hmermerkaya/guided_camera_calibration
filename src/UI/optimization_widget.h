#pragma once

#include "Optimization/optimizer.h"
#include <QWidget>

namespace Ui {
class Optimization_Widget;
}

class Optimization_Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Optimization_Widget(QWidget *parent = nullptr);
    ~Optimization_Widget();

    ///
    /// \brief setOptimizer
    /// \param optimizer
    ///
    void setOptimizer(const std::shared_ptr<Optimizer>& optimizer);

private:

    ///
    /// \brief setProperties
    /// \param list
    /// \param mode
    ///
    void setProperties(const std::vector<std::shared_ptr<Property> > &list,
                       const int& mode);

private:
    Ui::Optimization_Widget* ui_;

    ///
    std::shared_ptr<Optimizer> optimizer_;

    ///
    std::vector<std::shared_ptr<Property>> widget_list_intr_;

    ///
    std::vector<std::shared_ptr<Property>> widget_list_pose_;

    ///
    std::vector<std::shared_ptr<Property>> widget_list_general_;
};
