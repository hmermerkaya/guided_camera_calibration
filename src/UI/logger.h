#pragma once

#include <opencv2/opencv.hpp>
#include <QWidget>

const int LOG_NORMAL = 0;
const int LOG_WARNING = 1;
const int LOG_ERROR = 2;
const int LOG_APPEND = 3;

//define logger macros for different types of messages
#define ERROR(msg) emit log(msg, LOG_ERROR)
#define WARN(msg) emit log(msg, LOG_WARNING)
#define STATUS(msg) emit log(msg, LOG_NORMAL)
#define LOGMATRIX(msg, mat) emit logMat(msg, mat)

namespace Ui {
class Logger;
}

class Logger : public QWidget{

    Q_OBJECT

public:
    ///
    /// \brief Logger is the standard constructor
    /// \param parent
    ///
    explicit Logger(QWidget *parent = nullptr);

    ///
    ~Logger();

public slots:

    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(const QString& msg, const int& type);

    ///
    /// \brief logMat
    /// \param msg
    /// \param mat
    ///
    void logMat(const QString& msg, const cv::Mat& mat);

private:
    ///
    Ui::Logger* ui_;

};

