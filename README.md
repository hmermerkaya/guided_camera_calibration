# Guided_Camera_Calibration

A tool for an interactive calibration of a single camera or stereo camera device with a small baseline, e.g. a Kinect.

## Dependencies

- Qt6 (for GUI and generic webcam camera class)
- OpenCV >=4 (for pattern creation/detection)
- [Ceres](http://ceres-solver.org/) (for performing the parameter optimization)
- [NLopt](https://nlopt.readthedocs.io) (for the global search for the next best pattern pose)
- [Azure Kinect SDK](https://learn.microsoft.com/en-us/azure/kinect-dk/) (OPTIONAL - for Azure Kinect support)
- [freenect2](https://github.com/OpenKinect/libfreenect2) (OPTIONAL - for Kinect v2 support)
- [MVS](https://www.hikrobotics.com/en/machinevision/service/download) (OPTIONAL - for Hikrobot camera support)
- Contact me for the support of additional devices!

## Build

Use the usual steps to build the application...

```
git clone 
mkdir build
cd build
cmake ..
make -j16
```

...or open the CMakeLists.txt with an IDE of your choice (e.g. QtCreator if you are planning on modfying the GUI)

Note: Via the CMake flags USE_AZURE_KINECT, USE_KINECT_V2 and USE_HIKROBOT, the respective support of the different devices can be turned on/off.

## Usage

1. **Camera selection**:

   If you have more than one device connected, select the preferred one at start-up. Note: Make sure to enable the libraries required by your camera.

   ![Camera Selector](https://gitlab.com/ungetym/guided_camera_calibration/-/raw/main/Docu/00.jpg)

2. **Overview**:

   After the camera selection you should be greeted with the following window

   ![Initial View](https://gitlab.com/ungetym/guided_camera_calibration/-/raw/main/Docu/01_initial.jpg)On the left you find three tabs:

   i) Patterns: Here you can configure and preview your calibration pattern as well as create a pdf of it.

   ii) Optimization: Here you can find the parameters for the different optimization procedures, such as iteration steps.

   iii) Live: This tab shows the live camera feed and the AR overlay for the next pattern position proposal.
   
   On the right the current camera intrinsics and a logging window are shown.

3. **Calibration pattern configuration**:

   Start the calibration process by selecting the calibration pattern parameters in the parameters area shown above. Currently there are two supported pattern types, classical checkerboards and ChArUco-boards which is a checkerboard with additional ArUco markers useful for detecting partially occluded patterns. In general, we recommend using ChArUco-boards. However, the marker detection is difficult in cases of large FoV combined with a small resolution as e.g. in the IR cameras of Kinect devices. For these devices, we recommend using classical checkerboards.
   After clicking "Create Pattern" a preview will be shown in the middle area as shown here:

   ![Pattern Preview](https://gitlab.com/ungetym/guided_camera_calibration/-/raw/main/Docu/02_pattern.jpg)

4. **Print and measure**:

   After creating the pattern, print it (or order a high quality version in one of the various shops concerned with this topic) and measure the edge size of a checkerboard square. Put this value into the "Square Size (mm)" field.

5. **Optimization setup**:

   Switch to the "Optimization" tab and change the parameters to your liking.

   ![Optimization Tab](https://gitlab.com/ungetym/guided_camera_calibration/-/raw/main/Docu/03_optparams.jpg)
   The overall calibration procedure for a single camera is the following:

   i) The first n pattern pose proposals are randomly generated (with n="Number of Random Poses"). After all these proposals were matched by the user, the first Levenberg-Marquardt optimization starts. The number of threads used for the LM optimization as well as the maximum number of iterations can be configured as shown in the image above. Since a single optimization run sometimes fails due to bad starting values, you also have the option to allow re-runs with slightly randomized starting parameters. The number of these re-runs is specified by "Number of Optimization Runs". In addition you can select a non-zero "Gradient Tolerance" if you want to include this as additional LM stop criterium.

   ii) After this initial LM optimization, NLopt is used to perform a global optimization for the next best pattern pose based on the Jacobi matrix of the parameter optimization. This method is based on [Peng and Sturm - 'Calibration Wizard: A Guidance...'](https://openaccess.thecvf.com/content_ICCV_2019/papers/Peng_Calibration_Wizard_A_Guidance_System_for_Camera_Calibration_Based_on_ICCV_2019_paper.pdf). As with number of random poses, n, you can also specify m, the "Number of optimized Poses". This is not relevant for a single device calibration, but for stereo devices since that procedure switches to the next camera or optimization mode after n+m matched patterns. Furthermore, the number of optimization steps for NLopt can be set via "Number of Global Optimization Steps".

   iii) From now on, after a pose proposal is matched, the LM optimization is run to refine the intrinsics.
   
   For stereo devices, the two camera intrinsics are estimated separately. After the calibration procedure has been finished (n+m matched pattern poses) for the first camera, the tool switches to the second camera to perform that camera's intrinsics calibration. After further n+m matched poses, the tool then switches to the extrinsics mode in which only the transformation between the two cameras is estimated.

   A final bundle adjustment step is currently in development and not available yet.

6. **Match the proposals**:

   Go to the "Live" tab and start the camera stream. For some devices, the stream might not be controllable via the Start/Pause/Stop button due to driver issues. If you followed the previous steps and generated a calibration pattern, you should see the AR overlay showing the proposed pattern position:
   
   ![AR Overlay](https://gitlab.com/ungetym/guided_camera_calibration/-/raw/main/Docu/04_camview.jpg)
   
   In addition to the orange overlay you can also see the detected corners marked in green. Now you need to move the pattern to match the proposal, which is then confirmed by turning the overlay into a green one:
   
   ![Match](https://gitlab.com/ungetym/guided_camera_calibration/-/raw/main/Docu/05_accepted.jpg)
   
   As can be seen in the image above, there is a certain tolerance for the placement. To prevent motion blur affecting the optimization, the detector does not only check, how close the detected and proposed corners are to each other, but also the amount of movement between successful matches. Accordingly, you have to hold the pattern still for a second after moving it to the correct pose in order for the detections to get accepted. After this successful match, the next pose is created and the overlay will change.

   If you can't match the overlay, e.g. due to lighting/noise problems or a faulty proposal, click the "Next Pose" button. Also note, that you can mirror the camera feed via the checkbox according to your preference.

7. **Save everything**:

   If you are satisfied with the parameters and reprojection errors, stop matching the overlays and save the parameters either as .MCC_Cams file which is compatible with my [Multi-Camera Calibration](https://gitlab.com/ungetym/Multi_Camera_Calibration) tool or as simple .json file by clicking the "Save" button in the "Camera Parameters" area.
   

## Contact

Tim Michels
[tmi@informatik.uni-kiel.de](mailto:tmi@informatik.uni-kiel.de)